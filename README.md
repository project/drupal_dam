# Drupal Digital Asset Management

Trying to provide a Digital Asset Management with Drupal Media

This is alpha status, some more or less sophisticated configuration
is needed to make it work. will provide details about this later.

## Sub module drupal_dam_media_file_delete

This module extends media_file_delete module to work with Drupal DAM
media entities as well

currently requires this patch: https://www.drupal.org/project/media_file_delete/issues/3279220