<?php
/**
 * @file
 * Hooks and documentation related to drupal_dam module
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * alter the entity before it gets pushed to DAM
 * \Drupal\drupal_dam\Service\DAMJSONPushService
 *
 * @param \Drupal\Core\Entity\ContentEntityInterface $entity
 */
function hook_drupal_dam_push_entity_alter(\Drupal\Core\Entity\ContentEntityInterface &$entity) {
  // Add to taxonomy term reference field
  $entity->get('field_tags')->appendItem(['target_id' => 1]);
}

/**
 * alter the entity during field sync with DAM.
 *
 * see @\Drupal\drupal_dam\Service\FieldSyncService
 *
 * @param \Drupal\Core\Entity\ContentEntityInterface $entity
 * @param \Drupal\media\MediaTypeInterface $media_type
 * @param array $attributes_and_relations
 *
 * @return void
 */
function hook_drupal_dam_field_sync_alter(\Drupal\Core\Entity\ContentEntityInterface &$entity, \Drupal\media\MediaTypeInterface $media_type, array $attributes_and_relations) {
  // Add to taxonomy term reference field
  $entity->get('field_tags')->appendItem(['target_id' => 1]);
}

/**
 * alter JSON Request before sending to DAM host (JSON API).
 *
 * see @\Drupal\drupal_dam\Service\DAMJSONTalkService
 *
 * @param \Drupal\drupal_dam\Service\DAMJSONTalkService $jsonTalk
 *
 * @return void
 */
function hook_drupal_dam_json_request(\Drupal\drupal_dam\Service\DAMJSONTalkService $jsonTalk) {
  $jsonTalk->queryCondition('license-valid', 'field_media_license_invalid', 'TRUE', '<>');
}

/**
 * @} End of "addtogroup hooks".
 */
