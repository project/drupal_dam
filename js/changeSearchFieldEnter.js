(function ($, Drupal) {
  Drupal.behaviors.drupalDAMChangeSearchFieldEnterBehavior = {
    attach: function (context, settings) {

      $('input[data-dam-search-field="1"]').keypress(function (e) {
        if (e.which === 13) {
          $('input[data-dam-search-field="1"]').change();
          $('input[data-dam-search-field="1"]').blur();
          e.preventDefault();
        }
      });
    }
  };
})(jQuery, Drupal);
