(function ($, Drupal) {
Drupal.behaviors.drupalDAMChooseElementBehavior = {
  attach: function (context, settings) {
    // Disable submit button initially (nothing is selected)
    toggle_submit(false);
    var multiple = false;
    // check whether multiple selection are allowed
    if (typeof drupalSettings.drupal_dam !== 'undefined') {
      multiple = !!drupalSettings.drupal_dam.dam_browser_multiple;
    }
    // Selecting an asset
    $(once('damchoose', '.auswahlliste img.active')).click(function() {
      if (multiple) {
        $('*[data-damuuid="hidden-input"]').val(trim_string($('*[data-damuuid="hidden-input"]').val()+','+$(this).data("uuid")));
      }
      else {
        $('.auswahlliste .dam-item').removeClass('chooseborder');
        $('*[data-damuuid="hidden-input"]').val($(this).data("uuid"));
        $('*[data-damuuid="hidden-input-recreate"]').val('');
        $('.auswahlliste .dam-item .recreate').addClass('hidden');
      }
      $(this).parent().addClass('chooseborder');
      $(this).siblings().children('input').removeAttr('disabled');
      $(this).siblings('.recreate').removeClass('hidden');
      toggle_submit(true);
    });
    // Deselecting an asset
    $(once('damunchoose', '.auswahlliste .dam-item .dam-item-check')).click(function() {
      $(this).parent().removeClass('chooseborder');
      if (multiple) {
        $('*[data-damuuid="hidden-input"]').val(trim_string(remove_item($('*[data-damuuid="hidden-input"]').val(), $(this).parent().children('img').data("uuid"))));
        $('*[data-damuuid="hidden-input-recreate"]').val(trim_string(remove_item($('*[data-damuuid="hidden-input-recreate"]').val(), $(this).parent().children('img').data("uuid"))));
      }
      else {
        $('*[data-damuuid="hidden-input"]').val('');
        $('*[data-damuuid="hidden-input-recreate"]').val('');
      }
      $(this).siblings().children('input').attr('disabled', true);
      $(this).siblings('.recreate').addClass('hidden');
      if ($('*[data-damuuid="hidden-input"]').val().length === 0) {
        toggle_submit(false);
      }
    });
    // Handle recreation of existing assets
    $(once('damrecreeate', '.auswahlliste .dam-item input')).change(function() {
      var damItemStatus = $(this).prop("checked");
      if (damItemStatus) {
        if (multiple) {
          $('*[data-damuuid="hidden-input-recreate"]').val(trim_string($('*[data-damuuid="hidden-input-recreate"]').val()+','+$(this).parent().siblings('img').data('uuid')));
        }
        else {
          $('*[data-damuuid="hidden-input-recreate"]').val($(this).parent().siblings('img').data('uuid'));
        }
      }
      else {
        if (multiple) {
          $('*[data-damuuid="hidden-input-recreate"]').val(trim_string(remove_item($('*[data-damuuid="hidden-input-recreate"]').val(), $(this).parent().siblings('img').data('uuid'))));
        }
        else {
          $('*[data-damuuid="hidden-input-recreate"]').val('');
        }
      }
    });

    /**
     * toggle (enable/disable) submit button
     * @param enable
     */
    function toggle_submit(enable) {
      if (enable) {
        $('.is-entity-browser-submit#edit-submit').removeClass(['disabled-button', 'is-disabled']);
      }
      else {
        $('.is-entity-browser-submit#edit-submit').addClass(['disabled-button', 'is-disabled']);
      }

    }

    /**
     * trim string left and right from separator
     * @param string
     * @returns {*}
     */
    function trim_string(string) {
      string = string.replace(/^,+/, "");
      string = string.replace(/,+$/, "");
      return string;
    }

    /**
     * remove an item (UUID) from a string
     *
     * @param string
     * @param replace
     * @returns {*}
     */
    function remove_item(string, replace) {
      string = string.replace(replace, '');
      return string;
    }
  }
};
})(jQuery, Drupal);
