<?php

namespace Drupal\drupal_dam_host\FieldItemList;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\file\FileInterface;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;

/**
 * Provide file info for media entity
 */
class FileInfoFieldItemList extends FieldItemList {
  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $media = $this->getEntity();
    if ($media instanceof MediaInterface && !$media->isNew()) {
      $media_type = \Drupal::entityTypeManager()->getStorage('media_type')->load($media->bundle());
      if ($media_type instanceof MediaTypeInterface) {
        $source_field = $media->getSource()->getSourceFieldDefinition($media_type);
        if ($source_field && is_a($source_field->getItemDefinition()->getClass(), FileItem::class, TRUE)) {
          $file = \Drupal::entityTypeManager()->getStorage('file')->load($media->getSource()->getSourceFieldValue($media));
          if ($file instanceof FileInterface) {
            if ($this->getSetting('mime')) {
              $value = $file->getMimeType();
            }
            else {
              $value = $file->getSize();
            }
          }
        }
      }
    }
    if (!empty($value)) {
      $this->list[] = $this->createItem(0, $value);
    }
  }

}