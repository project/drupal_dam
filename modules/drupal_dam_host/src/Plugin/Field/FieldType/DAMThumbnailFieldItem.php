<?php

namespace Drupal\drupal_dam_host\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the 'dam_thumbnail' entity field type.
 *
 * @FieldType(
 *   id = "dam_thumbnail",
 *   label = @Translation("Thumbnail image style"),
 *   description = @Translation("An entity field containing a path to the thumbnail image style."),
 *   no_ui = TRUE,
 *   list_class = "\Drupal\drupal_dam_host\Plugin\Field\FieldType\DAMThumbnailItemList"
 * )
 */
class DAMThumbnailFieldItem extends FieldItemBase {

  /**
   * @inheritDoc
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    return [];
  }

  /**
   * @inheritDoc
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [];
  }

}
