<?php

namespace Drupal\drupal_dam_host\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\image\Entity\ImageStyle;

/**
 * Class DAMThumbnailItemList
 *
 * @package Drupal\drupal_dam_host\Plugin\Field\FieldType
 */
class DAMThumbnailItemList extends FieldItemList {

  use ComputedItemListTrait;

  protected function computeValue() {
    /* @var \Drupal\image\Entity\ImageStyle $media_library_style */
    $media_library_style = ImageStyle::load('media_library');
    if (!$this->getEntity()->thumbnail->isEmpty()) {
      $thumbnail_id = ($this->getEntity()->thumbnail->target_id);
      /* @var \Drupal\file\Entity\File $thumbnail_entity */
      $thumbnail_entity = \Drupal::entityTypeManager()
        ->getStorage('file')
        ->load($thumbnail_id);
      if ($thumbnail_entity) {
        $this->list[0] = $this->createItem(0, [
          'url' => $media_library_style->buildUrl($thumbnail_entity->getFileUri())
        ]);
      }
    }
  }

}
