<?php

namespace Drupal\drupal_dam_host\Plugin\Field\FieldWidget;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\media\MediaTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
/**
 * Plugin implementation of the 'dam_refresh_default' widget.
 *
 * @FieldWidget(
 *   id = "dam_refresh_default",
 *   label = @Translation("DAM refresh indicator"),
 *   field_types = {
 *     "integer"
 *   }
 * )
 */
class DAMRefreshDefaultWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $counter = empty($items[0]->value) ? 0 : $items[0]->value;
    /* @var \Drupal\media\Entity\MediaType $mediaTypeCurrent */
    try {
      $mediaTypeCurrent = $this->entityTypeManager->getStorage('media_type')
        ->load($items->getEntity()->bundle());
    } catch (InvalidPluginDefinitionException $e) {
    } catch (PluginNotFoundException $e) {
    }
    if (isset($mediaTypeCurrent) && $mediaTypeCurrent instanceof MediaTypeInterface && $mediaTypeCurrent->getThirdPartySetting('drupal_dam_host', 'damrefresh')) {
      $element['value'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('DAM Refresh'),
        '#return_value' => $counter + 1,
        '#current_refresh_value' => $counter
      ];
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // keep current value in field if checkbox is not checked
    if (empty($values[0]['value'])) {
      $values[0]['value'] = $form['dam_refresh_indicator']['widget'][0]['value']['#current_refresh_value'];
    }
    else {
      $this->messenger()->addMessage($this->t('Refresh has been indicated.'));
    }
    return parent::massageFormValues($values, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getFieldStorageDefinition()->getName() == 'dam_refresh_indicator';
  }

}
