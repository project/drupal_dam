<?php

namespace Drupal\drupal_dam_media_file_delete\Form;

use Drupal\drupal_dam\Plugin\media\Source\DAMFile;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for a media delete form.
 *
 */
class MediaDeleteForm extends \Drupal\media_file_delete\Form\MediaDeleteForm {

  /**
   * {@inheritdoc}
   */
  protected function getFile(MediaInterface $media): ?FileInterface {
    $file = parent::getFile($media);
    if (empty($file)) {
      $source = $media->getSource();
      if ($source instanceof DAMFile) {
        $media_type = $this->entityTypeManager->getStorage('media_type')->load($media->bundle());
        if ($media_type instanceof MediaTypeInterface) {
          $field_name = $source->getLocalCopyFieldDefinition($media_type)
            ->getName();
          if ($media->hasField($field_name) && !empty($media->get($field_name))) {
            return File::load(($media->get($field_name)->target_id));
          }
        }
      }
    }
    return $file;
  }

}
