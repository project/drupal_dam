<?php

namespace Drupal\drupal_dam_media_file_delete\Form;

use Drupal\drupal_dam\Plugin\media\Source\DAMFile;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;

class MediaDeleteMultipleForm extends \Drupal\media_file_delete\Form\MediaDeleteMultipleForm {

  /**
   * {@inheritdoc}
   */
  protected function getFile(MediaInterface $media) : ?FileInterface {
    if ($media->getSource() instanceof DAMFile) {
      $media_type = $this->entityTypeManager->getStorage('media_type')->load($media->bundle());
      if ($media_type instanceof MediaTypeInterface) {
        $field_name = $media->getSource()->getLocalCopyFieldDefinition($media_type)
          ->getName();
        if ($media->hasField($field_name) && !empty($media->get($field_name))) {
          return File::load(($media->get($field_name)->target_id));
        }
      }
    }
    else {
      $field = $media->getSource()
        ->getSourceFieldDefinition($media->bundle->entity);
      if (is_a($field->getItemDefinition()
        ->getClass(), FileItem::class, TRUE)) {
        $fid = $media->getSource()->getSourceFieldValue($media);
        if ($fid) {
          return File::load($fid);
        }
      }
    }
    return NULL;
  }
}