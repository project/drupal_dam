<?php

/**
 * @file
 * Hooks related to DAM refresh
 */

/**
 * @addtogroup hooks
 * @{
 */

use Drupal\media\Entity\Media;

/**
 * add fields by name that should be synced during DAM refresh
 *
 * by default no fields are synced during DAM refresh (only file and
 * file meta data), fields need to be defined in the DAM sync form display too.
 *
 * @param $fields array
 *   fields that should be synced during DAM refresh
 * @param $media \Drupal\media\Entity\Media
 *   media entity context
 */
function hook_damrefresh_fields_alter(&$fields, Media $media) {
  if ($media->bundle() == 'image') {
    $fields[] = 'field_name';
  }
}

/**
 * @} End of "addtogroup hooks"
 */