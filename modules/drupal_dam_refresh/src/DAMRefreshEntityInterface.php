<?php

namespace Drupal\drupal_dam_refresh;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Interface DAMRefreshEntityInterface
 *
 * @package Drupal\drupal_dam_refresh
 */
interface DAMRefreshEntityInterface extends ContentEntityInterface, EntityChangedInterface {

}