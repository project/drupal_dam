<?php

namespace Drupal\drupal_dam_refresh;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\media\Entity\Media;

/**
 * Class DAMRefreshEntityListBuilder
 *
 * @package Drupal\drupal_dam_refresh
 */
class DAMRefreshEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['id'] = $this->t('DAM Refresh ID');
    $header['local_asset'] = $this->t('Local asset');
    $header['version'] = $this->t('Version');
    $header['refreshed'] = $this->t('Refreshed');
    $header['refresh'] = $this->t('Refresh');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\drupal_dam_refresh\Entity\DAMRefreshEntity $entity */
    if ($entity->refreshed->value == 0) {
      $row = [];
      $row['id'] = $entity->id();
      $localAsset = \Drupal::entityTypeManager()
        ->getStorage('media')
        ->load($entity->local_asset->target_id);
      $localAssetLoaded = $localAsset instanceof Media;
      $row['local_asset'] = $localAssetLoaded ? $localAsset->toLink($localAsset->getName(), 'edit-form') : $this->t('Entity does not exist anymore locally');
      $row['version'] = $entity->version->value;
      $row['refreshed'] = $entity->refreshed->value;
      $row['refresh'] = $entity->refreshed->value == 0 && $localAssetLoaded ? Link::createFromRoute($this->t('Refresh'), 'damrefresh.refresh', ['damrefresh' => $entity->id()]) : '';
      return $row + parent::buildRow($entity);
    }
    return [];
  }

}
