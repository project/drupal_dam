<?php

namespace Drupal\drupal_dam_refresh;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

class DAMRefreshHtmlRouteProvider extends AdminHtmlRouteProvider {


  /**
   * {@inheritdoc}
   *
   * add custom entity route for refreshing entities
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($refresh_form_route = $this->getRefreshFormRoute($entity_type)) {
      $collection->add("$entity_type_id.refresh", $refresh_form_route);
    }

    if ($delete_route = $this->getDeleteFormRoute($entity_type)) {
      $collection->add("{$entity_type_id}.delete_form", $delete_route);
    }

    return $collection;
  }

  /**
   * {@inheritdoc}
   *
   * build the refresh route
   */
  protected function getRefreshFormRoute(EntityTypeInterface $entityType) {
    $entity_type_id = $entityType->id();
    $route = new Route($entityType->getLinkTemplate('refresh-form'));
    $route
      ->addDefaults([
      '_entity_form' => "{$entity_type_id}.refresh"
      ])
      ->setRequirement('_entity_access', "{$entity_type_id}.refresh")
      ->setOption( 'parameters', [
        $entity_type_id => ['type' => 'entity:' . $entity_type_id]
      ]);
    return $route;
  }

}