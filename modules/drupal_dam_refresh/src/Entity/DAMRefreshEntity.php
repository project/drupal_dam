<?php

namespace Drupal\drupal_dam_refresh\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\drupal_dam_refresh\DAMRefreshEntityInterface;

/**
 * Defines the DAM Refresh Entity.
 *
 * @ingroup damrefresh
 *
 * @ContentEntityType(
 *   id = "damrefresh",
 *   label = @Translation("DAM Refresh"),
 *   admin_permission = "use dam refresh",
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\drupal_dam_refresh\DAMRefreshEntityListBuilder",
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\drupal_dam_refresh\DAMRefreshHtmlRouteProvider"
 *     },
 *     "form" = {
 *       "refresh" = "Drupal\drupal_dam_refresh\Form\DAMRefreshRefreshForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     }
 *   },
 *   base_table = "damrefresh",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "refresh-form" = "/admin/content/damrefresh/{damrefresh}/refresh",
 *     "collection" = "/admin/content/damrefresh",
 *     "delete-form" = "/admin/content/damrefresh/{damrefresh}/delete",
 *   }
 * )
 */
class DAMRefreshEntity extends ContentEntityBase implements DAMRefreshEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the DAM refresh entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the DAM refresh entity.'))
      ->setReadOnly(TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time the refresh entity was last changed.'));

    $fields['local_asset'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Local asset'))
      ->setSetting('target_type', 'media');

    $fields['version'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Version of refresh'));

    $fields['refreshed'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Refresh has been done'));

    return $fields;
  }

}