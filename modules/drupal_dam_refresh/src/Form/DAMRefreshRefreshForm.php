<?php

namespace Drupal\drupal_dam_refresh\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DAMRefreshRefreshForm
 *
 * @ingroup damrefresh
 *
 * @package Drupal\drupal_dam_refresh\Form
 */
class DAMRefreshRefreshForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to refresh the @entity-type %label?', [
      '@entity-type' => $this->getEntity()->getEntityType()->getSingularLabel(),
      '%label' => $this->getEntity()->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $entity = $this->getEntity();
    return $entity->toUrl('collection');
  }

  /**
   * {@inheritdoc}
   *
   * update referenced media entity and damrefresh entity status
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /* @var \Drupal\drupal_dam_refresh\Entity\DAMRefreshEntity $entity */
    $entity = $this->getEntity();
    $referenceId = $entity->local_asset->target_id;
    /* @var \Drupal\media\Entity\Media $reference */
    $reference = $this->entityTypeManager->getStorage('media')->load($referenceId);
    /* @var \Drupal\drupal_dam_refresh\Service\DAMRefreshService $refreshService */
    $refreshService = \Drupal::service('drupal_dam_refresh.refresh');
    if ($refreshService->refresh($reference)) {
      $entity->set('refreshed', 1);
      try {
        $entity->save();
        $this->messenger()->addStatus($this->t('Refreshed this asset.'));
      } catch (EntityStorageException $e) {
        $this->messenger()->addError($this->t('Error refreshing.'));
      }
    }
    else {
      $this->messenger()->addError($this->t('Error refreshing this asset.'));
    }
    $form_state->setRedirectUrl($entity->toUrl('collection'));
  }

}