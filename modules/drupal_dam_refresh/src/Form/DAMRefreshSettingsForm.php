<?php

namespace Drupal\drupal_dam_refresh\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DAMRefreshSettingsForm
 *
 *
 * @package Drupal\drupal_dam_refresh\Form
 */
class DAMRefreshSettingsForm extends ConfigFormBase {

  const SETTINGS = 'drupal_dam_refresh.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drupal_dam_refresh_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('drupal_dam_refresh.settings');

    $form['refresh_redirect'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Redirect on login to refresh asset collection'),
      '#default_value' => $config->get('refresh_redirect')
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('refresh_redirect', $form_state->getValue('refresh_redirect'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}