<?php

namespace Drupal\drupal_dam_refresh\Service;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\drupal_dam\Service\DAMJSONTalkService;
use Drupal\drupal_dam_refresh\Entity\DAMRefreshEntity;
use Drupal\media\Entity\Media;

/**
 * Class DAMRefreshEntitiesService
 *
 * @package Drupal\drupal_dam_refresh\Service
 */
class DAMRefreshEntitiesService {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityBundleInfo;

  /**
   * @var \Drupal\drupal_dam\Service\DAMJSONTalkService
   */
  protected $jsonTalk;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * DAMRefreshEntitiesService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_bundle_info
   * @param \Drupal\drupal_dam\Service\DAMJSONTalkService $json_talk
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_bundle_info, DAMJSONTalkService $json_talk, MessengerInterface $messenger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityBundleInfo = $entity_bundle_info;
    $this->jsonTalk = $json_talk;
    $this->messenger = $messenger;
  }

  /**
   * ToDo: Need some kind of batch process here
   *
   * checking with host for assets that need to be refreshed
   *
   * generating DAMRefreshEntity entities for each asset that needs to be
   * refreshed
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function checkRefresh() {
    $media_bundles = $this->entityBundleInfo->getBundleInfo('media');
    foreach ($media_bundles as $bundle_name => $bundle) {
      /* @var \Drupal\media\Entity\MediaType $mediaBundle */
      $mediaBundle = $this->entityTypeManager->getStorage('media_type')
        ->load($bundle_name);
      $mediaSource = $mediaBundle->getSource();
      if ($mediaSource->getPluginId() == 'dam_file' || $mediaSource->getPluginId() == 'dam_image') {
        $mediaSourceConfiguration = $mediaSource->getConfiguration();
        /* @var \Drupal\drupal_dam\Service\DAMJSONTalkService $jsonTalk */
        $mediaAsset = Media::create([
          'bundle' => $bundle_name
        ]);
        $this->jsonTalk->unsetConditions();
        $this->jsonTalk->setMediaBundle($mediaAsset);
        $this->jsonTalk->setMediaBundleDestination($mediaSourceConfiguration['dam_settings']['dam_mediabundle']);
        $this->jsonTalk->queryCondition('dam-refresh', 'dam_refresh_indicator', '1', '>=', 'filter');
        $response = $this->jsonTalk->doJSONRequest();

        if (!empty($response->body->data)) {
          foreach ($response->body->data as $refresh_item) {
            if (!empty($version = $refresh_item->attributes->dam_refresh_indicator)) {
              $localAsset = $this->entityTypeManager->getStorage('media')
                ->loadByProperties(
                  [
                    $mediaSourceConfiguration['source_field'] => $refresh_item->id
                  ]
                );
              if (!empty($localAsset)) {
                foreach ($localAsset as $asset) {
                  $damRefresher = $this->entityTypeManager->getStorage('damrefresh')
                    ->loadByProperties(
                      ['local_asset' => ['target_id' => $asset->id()]]
                    );
                  if (!$damRefresher) {
                    $damRefresher = DAMRefreshEntity::create([
                      'version' => $version,
                      'refreshed' => FALSE,
                      'local_asset' => [
                        'target_id' => $asset->id()
                      ]
                    ]);
                    $damRefresher->save();
                  }
                  else {
                    $damRefresher = array_pop($damRefresher);
                    if ($damRefresher->version->value != $version) {
                      $damRefresher->set('version', $version);
                      $damRefresher->set('refreshed', 0);
                      $damRefresher->save();
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
