<?php

namespace Drupal\drupal_dam_refresh\Service;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\drupal_dam\Service\DAMFileRetrieveService;
use Drupal\drupal_dam\Service\DAMJSONTalkService;
use Drupal\drupal_dam\Service\FieldSyncService;
use Drupal\media\Entity\Media;

/**
 * Class DAMRefreshService
 *
 * @package Drupal\drupal_dam_refresh\Service
 */
class DAMRefreshService {

  /**
   * @var \Drupal\drupal_dam\Service\DAMFileRetrieveService
   */
  protected $fileRetrieveService;

  /**
   * @var \Drupal\drupal_dam\Service\DAMJSONTalkService
   */
  protected $jsonTalk;

  /**
   * @var \Drupal\drupal_dam\Service\FieldSyncService
   */
  protected $fieldSyncService;

  /**
   * DAMRefreshService constructor.
   *
   * @param \Drupal\drupal_dam\Service\DAMFileRetrieveService $file_retrieve_service
   * @param \Drupal\drupal_dam\Service\DAMJSONTalkService $json_talk
   * @param \Drupal\drupal_dam\Service\FieldSyncService $field_sync_service
   */
  public function __construct(DAMFileRetrieveService $file_retrieve_service, DAMJSONTalkService $json_talk, FieldSyncService $field_sync_service) {
    $this->fileRetrieveService = $file_retrieve_service;
    $this->jsonTalk = $json_talk;
    $this->fieldSyncService = $field_sync_service;
  }

  /**
   * refreshing media entity from host
   *
   * update associated file of referenced media entity (no other meta data
   * at this moment).
   *
   * @param \Drupal\media\Entity\Media $media
   *
   * @return bool|\Drupal\file\FileInterface|mixed
   */
  public function refresh(Media $media) {
    $fields = [];
    \Drupal::moduleHandler()->alter('damrefresh_fields', $fields, $media);
    if (!empty($fields)) {
      $this->fieldSyncService->initFieldSync($media, $fields);
    }
    $file = $this->fileRetrieveService->retrieveURL($media);
    $fid =  $file->fid->value;
    $source_conf = $media->getSource()->getConfiguration();
    $media->{$source_conf['localcopy_field']}->target_id = $fid;
    $this->jsonTalk->setMediaBundle($media);
    $response = $this->jsonTalk->doJSONRequest();
    $damSourceField = $this->jsonTalk->getDAMMediaSourceField();
    if ($media->getSource()->getPluginId() == 'dam_image') {
      $media->thumbnail->target_id = $fid;
      $MetaDam = $response->body->data->relationships->{$damSourceField}->data->meta;
      $media->{$source_conf['localcopy_field']}->alt = $media->thumbnail->alt = $MetaDam->alt;
      $media->{$source_conf['localcopy_field']}->title = $media->thumbnail->title = $MetaDam->title;
      $media->{$source_conf['localcopy_field']}->width = $media->thumbnail->width = $MetaDam->width;
      $media->{$source_conf['localcopy_field']}->height = $media->thumbnail->height = $MetaDam->height;
    }

    try {
      $media->save();
      return TRUE;
    } catch (EntityStorageException $e) {
      return FALSE;
    }
  }

}