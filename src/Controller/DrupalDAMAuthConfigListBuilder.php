<?php

namespace Drupal\drupal_dam\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

class DrupalDAMAuthConfigListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Name');
    $header['id'] = $this->t('Machine name');
    $header['auth_url'] = $this->t('URL of authentication server');
    $header['client_id'] = $this->t('OAuth client id');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\drupal_dam\Entity\DrupalDAMAuthConfig $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['auth_url'] = $entity->auth_url;
    $row['client_id'] = $entity->client_id;

    return $row + parent::buildRow($entity);
  }

}