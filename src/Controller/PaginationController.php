<?php

namespace Drupal\drupal_dam\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\drupal_dam\ControllerSupport;
use Drupal\drupal_dam\Service\JSONRequestService;
use Drupal\drupal_dam\Service\ResponseFormatterService;
use Drupal\media\Entity\MediaType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PaginationController
 *
 * @package Drupal\drupal_dam\Controller
 */
class PaginationController extends ControllerBase {

  /**
   * @var \Drupal\drupal_dam\Service\JSONRequestService
   */
  protected $jsonRequest;

  /**
   * @var \Drupal\drupal_dam\Service\ResponseFormatterService
   */
  protected $responseFormatter;

  /**
   * PaginationController constructor.
   *
   * @param \Drupal\drupal_dam\Service\JSONRequestService $json_request
   * @param \Drupal\drupal_dam\Service\ResponseFormatterService $response_formatter
   */
  public function __construct(JSONRequestService $json_request, ResponseFormatterService $response_formatter) {
    $this->jsonRequest = $json_request;
    $this->responseFormatter = $response_formatter;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\drupal_dam\Controller\PaginationController|static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('drupal_dam.jsonrequest'),
      $container->get('drupal_dam.responseformatter')
    );
  }

  /**
   * return the requested page from pager via Ajax and unset value if set
   *
   * @param \Drupal\media\Entity\MediaType $mediatype
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function ajaxpagination(MediaType $mediatype) {
    $paginationurl = \Drupal::request()->query->get('pagination');
    $useExisting = boolval(\Drupal::request()->query->get('useexisting'));
    $allowRecreation = boolval(\Drupal::request()->query->get('allowrecreation'));
    $paginationCount = \Drupal::request()->query->get('pagination_count');
    $reset = boolval(\Drupal::request()->query->get('reset'));
    $this->jsonRequest->setRequestURI($paginationurl);
    $this->jsonRequest->doRequest();
    $this->responseFormatter->setResponse($this->jsonRequest->getResponseBody(), $mediatype);
    if (!$allowRecreation) {
      $this->responseFormatter->setAllowRecreation($allowRecreation);
    }
    if ($reset) {
      $this->responseFormatter->setReset($reset);
    }
    if ($paginationCount) {
      $this->responseFormatter->setPaginationCount($paginationCount);
    }
    $element = $this->responseFormatter->getItemList($useExisting);
    return ControllerSupport::renderAjaxDAMItemList($element);
  }
}
