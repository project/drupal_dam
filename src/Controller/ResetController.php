<?php

namespace Drupal\drupal_dam\Controller;

use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\drupal_dam\ControllerSupport;
use Drupal\drupal_dam\Service\DAMJSONTalkService;
use Drupal\drupal_dam\Service\ResponseFormatterService;
use Drupal\media\Entity\MediaType;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ResetController extends ControllerBase {

  /**
   * @var \Drupal\drupal_dam\Service\DAMJSONTalkService
   */
  protected $jsonTalk;

  /**
   * @var \Drupal\drupal_dam\Service\ResponseFormatterService
   */
  protected $responseFormatter;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ResetController constructor.
   *
   * @param \Drupal\drupal_dam\Service\DAMJSONTalkService $json_talk
   * @param \Drupal\drupal_dam\Service\ResponseFormatterService $response_formatter
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(DAMJSONTalkService $json_talk, ResponseFormatterService $response_formatter, EntityTypeManagerInterface $entity_type_manager) {
    $this->jsonTalk = $json_talk;
    $this->responseFormatter = $response_formatter;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\drupal_dam\Controller\ResetController|static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('drupal_dam.jsontalk'),
      $container->get('drupal_dam.responseformatter'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * @param \Drupal\media\Entity\MediaType $mediatype
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function reset(MediaType $mediatype) {
    $media = $this->entityTypeManager->getStorage('media')->create(['bundle' => $mediatype->id()]);
    $mediaTypeSourceConfig = $media->getSource()->getConfiguration();
    $pagination = \Drupal::request()->query->get('pagination');
    $useExisting = boolval(\Drupal::request()->query->get('useexisting'));
    $allowRecreation = boolval(\Drupal::request()->query->get('allowrecreation'));
    $this->jsonTalk->setMediaBundle($media);
    if ($mediaTypeSourceConfig['dam_search']['enabled']) {
      $this->jsonTalk->setSearchAPIIndex();
      $this->jsonTalk->queryCondition('bundle', FALSE, $mediaTypeSourceConfig['dam_settings']['dam_mediabundle']);
      $this->jsonTalk->querySort('default-sort', $mediaTypeSourceConfig['dam_sort']['path'], $mediaTypeSourceConfig['dam_sort']['direction']);
    }
    $this->jsonTalk->queryPageLimit($pagination);
    $response = $this->jsonTalk->doJSONRequest();
    $this->responseFormatter->setResponse($response, $mediatype);
    if (!$allowRecreation) {
      $this->responseFormatter->setAllowRecreation($allowRecreation);
    }
    $this->responseFormatter->setPaginationCount($pagination);
    $element = $this->responseFormatter->getItemList($useExisting);
    $ajaxResponse = ControllerSupport::renderAjaxDAMItemList($element);
    $ajaxResponse->addCommand(new InvokeCommand('.dam-search', 'val', ['']));
    return $ajaxResponse;
  }

}
