<?php

namespace Drupal\drupal_dam\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\drupal_dam\ControllerSupport;
use Drupal\drupal_dam\Service\DAMJSONTalkService;
use Drupal\drupal_dam\Service\ResponseFormatterService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SearchController extends ControllerBase {

  /**
   * @var \Drupal\drupal_dam\Service\DAMJSONTalkService
   */
  protected $jsonTalk;

  /**
   * @var \Drupal\drupal_dam\Service\ResponseFormatterService
   */
  protected $responseFormatter;

  /**
   * SearchController constructor.
   *
   * @param \Drupal\drupal_dam\Service\DAMJSONTalkService $json_talk
   * @param \Drupal\drupal_dam\Service\ResponseFormatterService $response_formatter
   */
  public function __construct(DAMJSONTalkService $json_talk, ResponseFormatterService $response_formatter) {
    $this->jsonTalk = $json_talk;
    $this->responseFormatter = $response_formatter;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\drupal_dam\Controller\SearchController|static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('drupal_dam.jsontalk'),
      $container->get('drupal_dam.responseformatter')
    );
  }

  /**
   * do search API search via JSON and return results as ajax list
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function doSearchAPISearch(array &$form, FormStateInterface $form_state) {
    $searchString = $form_state->getValue('search');
    if (empty($searchString)) {
      $values = $form_state->getValue($form_state->get('media_field_name'));
      if (empty($values)) {
        $values = $form_state->getValue([
          'inline_entity_form',
          $form_state->get('media_field_name')
        ]);
      }
      $searchString = $values[0]['search'];
    }
    $use_existing = $form_state->getValue('use_existing') ?? FALSE;
    $pagination = $form_state->getValue('dam_pagination') ?? 10;
    $mediaTypeBundle = $form_state->getValue('attached_entity')->bundle();//$form_state->getFormObject()->getEntity()->bundle();
    /* @var \Drupal\media\Entity\MediaType $mediaType */
    $mediaType = \Drupal::entityTypeManager()->getStorage('media_type')->load($mediaTypeBundle);
    $mediaTypeSourceConfig =  $mediaType->getSource()->getConfiguration();
    /* @var \Drupal\drupal_dam\Service\DAMJSONTalkService $jsonTalk */
    $jsonTalk = \Drupal::service('drupal_dam.jsontalk');
    $jsonTalk->unsetConditions();
    $jsonTalk->setSearchAPIIndex();
    $jsonTalk->queryPageLimit($pagination);
    $jsonTalk->queryCondition('fulltext', FALSE, $searchString);
    $jsonTalk->queryCondition('bundle', FALSE, $mediaTypeSourceConfig['dam_settings']['dam_mediabundle']);
    $jsonTalk->querySort('default-sort', $mediaTypeSourceConfig['dam_sort']['path'], $mediaTypeSourceConfig['dam_sort']['direction']);
    $response = $jsonTalk->doJSONRequest();
    /* @var \Drupal\drupal_dam\Service\ResponseFormatterService $responseFormatter */
    $responseFormatter = \Drupal::service('drupal_dam.responseformatter');
    $responseFormatter->setResponse($response, $mediaType);
    $responseFormatter->setReset(TRUE);
    $responseFormatter->setPaginationCount($pagination);
    $element = $responseFormatter->getItemList($use_existing);
    return ControllerSupport::renderAjaxDAMItemList($element);
  }

}