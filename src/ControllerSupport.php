<?php

namespace Drupal\drupal_dam;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * Class ControllerSupport
 *
 *   refactoring some code for reuse in different controllers
 *
 * @package Drupal\drupal_dam
 */
class ControllerSupport {

  /**
   * returning a new result set from DAM by Ajax
   *
   * @param $element
   *   render element
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public static function renderAjaxDAMItemList($element) {
    $ajaxResponse = new AjaxResponse();
    $ajaxResponse->addCommand(new ReplaceCommand('.auswahlliste', \Drupal::service('renderer')->render($element)));
    $ajaxResponse->addCommand(new InvokeCommand('*[data-damuuid="hidden-input"]', 'val', ['']));
    $ajaxResponse->addCommand(new InvokeCommand('*[data-damuuid="hidden-input-recreate"]', 'val', ['']));
    $ajaxResponse->addCommand(new InvokeCommand('.is-entity-browser-submit#edit-submit', 'addClass', ['disabled-button is-disabled']));
    return $ajaxResponse;
  }
}