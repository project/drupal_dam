<?php

namespace Drupal\drupal_dam;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 *
 * Reusable code parts for DAM browser widgets
 *
 * @package Drupal\drupal_dam
 */
trait DAMBrowserWidgetTrait {

  use StringTranslationTrait;

  /**
   * get search button for DAM browser widget
   *
   * @return array
   */
  public function getSearchButton($description = '') {
    return [
      '#type' => 'textfield',
      '#title' => $this->t('Search'),
      '#description' => $description,
      '#weight' => -1,
      '#attributes' => [
        'class' => ['dam-search'],
        'data-dam-search-field' => 1,
      ],
      '#ajax' => [
        'callback' => 'Drupal\drupal_dam\Controller\SearchController::doSearchAPISearch',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL
        ]
      ],
    ];
  }
}
