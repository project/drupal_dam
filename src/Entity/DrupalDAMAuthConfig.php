<?php

namespace Drupal\drupal_dam\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Example entity.
 *
 * @ConfigEntityType(
 *   id = "drupal_dam_auth",
 *   label = @Translation("DAM Authentication config"),
 *   handlers = {
 *     "list_builder" = "Drupal\drupal_dam\Controller\DrupalDAMAuthConfigListBuilder",
 *     "form" = {
 *       "add" = "Drupal\drupal_dam\Form\DrupalDAMAuthConfigForm",
 *       "edit" = "Drupal\drupal_dam\Form\DrupalDAMAuthConfigForm",
 *       "delete" = "Drupal\drupal_dam\Form\DrupalDAMAuthConfigDeleteForm",
 *     }
 *   },
 *   config_prefix = "drupal_dam_auth",
 *   admin_permission = "administer DAM settings",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "auth_url",
 *     "client_id",
 *     "client_secret"
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/services/dam-auth/{drupal_dam_auth}",
 *     "delete-form" = "/admin/config/services/dam-auth/{drupal_dam_auth}/delete",
 *   }
 * )
 */
class DrupalDAMAuthConfig extends ConfigEntityBase implements DrupalDAMAuthConfigInterface {

  protected $id;

  protected $label;

  public $auth_url;

  public $client_id;

  public $client_secret;


}