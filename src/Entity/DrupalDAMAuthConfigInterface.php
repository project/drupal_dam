<?php

namespace Drupal\drupal_dam\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface DrupalDAMAuthConfigInterface
 *
 * @package Drupal\drupal_dam\Entity
 */
interface DrupalDAMAuthConfigInterface extends ConfigEntityInterface {

}