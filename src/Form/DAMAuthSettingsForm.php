<?php

namespace Drupal\drupal_dam\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class DAMAuthSettingsForm extends ConfigFormBase {

  const SETTINGS = 'drupal_dam.auth';

  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  public function getFormId() {
    return 'drupal_dam_auth_settings';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['auth_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable authentication'),
      '#default_value' => $config->get('auth_enabled')
    ];

    $form['auth_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('DAM host url'),
      '#default_value' => $config->get('auth_url'),
      '#states' => [
        'visible' => [
          ':input[name="auth_enabled"]' => ['checked' => TRUE]
        ]
      ]
    ];

    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
      '#states' => [
        'visible' => [
          ':input[name="auth_enabled"]' => ['checked' => TRUE]
        ]
      ]
    ];

    $form['client_secret'] = [
      '#type' => 'password',
      '#title' => $this->t('Client secret'),
      '#states' => [
        'visible' => [
          ':input[name="auth_enabled"]' => ['checked' => TRUE]
        ]
      ]
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('auth_enabled', $form_state->getValue('auth_enabled'))
      ->set('auth_url', $form_state->getValue('auth_url'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->save();

    parent::submitForm($form, $form_state);
  }



}
