<?php

namespace Drupal\drupal_dam\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\drupal_dam\DAMBrowserWidgetTrait;
use Drupal\drupal_dam\Service\DAMJSONTalkService;
use Drupal\drupal_dam\Service\ResponseFormatterService;
use Drupal\media_library\Form\AddFormBase;
use Drupal\media_library\MediaLibraryUiBuilder;
use Drupal\media_library\OpenerResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DAMFileForm for Media Library Add
 *
 * @package Drupal\drupal_dam\Form
 */
class DAMFileForm extends AddFormBase {

  use DAMBrowserWidgetTrait;
  /**
   * @var \Drupal\drupal_dam\Service\DAMJSONTalkService
   */
  protected $jsonTalk;

  /**
   * @var \Drupal\drupal_dam\Service\ResponseFormatterService
   */
  protected $responseFormatter;


  public function __construct(EntityTypeManagerInterface $entity_type_manager, MediaLibraryUiBuilder $library_ui_builder, DAMJSONTalkService $json_talk, ResponseFormatterService $response_formatter, OpenerResolverInterface $opener_resolver = NULL) {
    parent::__construct($entity_type_manager, $library_ui_builder, $opener_resolver);
    $this->jsonTalk = $json_talk;
    $this->responseFormatter = $response_formatter;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('media_library.ui_builder'),
      $container->get('drupal_dam.jsontalk'),
      $container->get('drupal_dam.responseformatter'),
      $container->get('media_library.opener_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->getBaseFormId() . '_damfile';
  }

  /**
   * {@inheritdoc}
   */
  protected function buildInputElement(array $form, FormStateInterface $form_state) {
    $media_type = parent::getMediaType($form_state);
    $json_talk = $this->jsonTalk;
    /* @var \Drupal\media\Entity\Media $media */
    $media = $this->entityTypeManager->getStorage('media')->create(['bundle' => $media_type->id()]);
    $sourceConfig = $media->getSource()->getConfiguration();
    $json_talk->setMediaBundle($media);
    $json_talk->queryPageLimit(4);
    $response = $json_talk->doJSONRequest();
    $this->responseFormatter->setResponse($response, $media_type);
    $form['damselection'] = $this->responseFormatter->getItemList();

    $form['damuuid'] = [
        '#type' => 'hidden',
        '#attributes' => [
          'data-damuuid' => 'hidden-input'
        ]
      ];
    $form['damuuid']['#attached']['library'][] = 'drupal_dam/damgrid';
    $form['damuuid']['#attached']['library'][] = 'drupal_dam/damchoose';
    $form['damuuid']['#attached']['library'][] = 'drupal_dam/damsearchenter';

    // Add a container to group the input elements for styling purposes.
    $form['container'] = [
      '#type' => 'container',
    ];
    if ($sourceConfig['dam_search']['enabled']) {
      $form['search'] = $this->getSearchButton($sourceConfig['dam_search']['search_description']);
      $form['search']['#ajax']['url'] = Url::fromRoute('media_library.ui');
      $form['search']['#ajax']['options'] = [
        'query' => $this->getMediaLibraryState($form_state)->all() + [
            FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
          ]
      ];
      $form_state->setValue('attached_entity', $media);
      $form_state->setValue('dam_pagination', 4);
      $form_state->setValue('use_existing', TRUE);
    }

    $form['container']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#button_type' => 'primary',
      '#validate' => ['::validateDAM'],
      '#submit' => ['::addButtonSubmit'],
      // @todo Move validation in https://www.drupal.org/node/2988215
      '#ajax' => [
        'callback' => '::updateFormCallback',
        'wrapper' => 'media-library-wrapper',
        // Add a fixed URL to post the form since AJAX forms are automatically
        // posted to <current> instead of $form['#action'].
        // @todo Remove when https://www.drupal.org/project/drupal/issues/2504115
        //   is fixed.
        'url' => Url::fromRoute('media_library.ui'),
        'options' => [
          'query' => $this->getMediaLibraryState($form_state)->all() + [
              FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
            ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * validation callback, do not allow empty value
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateDAM(array &$form, FormStateInterface $form_state) {
    $uuid = $form_state->getValue('damuuid');
    if (empty($uuid)) {
      $form_state->setErrorByName('damuuid', t('Please choose a media item.'));
    }
  }

  /**
   * Submit handler for the add button.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function addButtonSubmit(array $form, FormStateInterface $form_state) {
    $this->processInputValues([$form_state->getValue('damuuid')], $form, $form_state);
  }
}
