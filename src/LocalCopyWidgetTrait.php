<?php

namespace Drupal\drupal_dam;

use Drupal\Core\Form\FormStateInterface;

/**
 * Trait LocalCopyWidgetTrait
 *
 * Reusable code parts for LocalCopy File widgets
 *
 * @package Drupal\drupal_dam
 */
trait LocalCopyWidgetTrait {

  /**
   * custom form element manipulations to prevent local file UI editing
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   * @param $delta
   * @param array $element
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function localCopyFormElement(\Drupal\Core\Field\FieldItemListInterface $items, $delta, array $element, array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    if ($items->isEmpty()) {
      $element = [
        '#type' => 'hidden',
        '#required' => $element['#required'],
        '#upload_validators' => $element['#upload_validators'],
        '#value' => NULL
      ];
      return $element;
    }
    $element['#after_build'][] = [get_class($this), 'unsetRemoveButton'];
    return $element;
  }

  /**
   * after_build callback to disable remove button while editing
   *
   * @param $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   */
  public static function unsetRemoveButton($element, FormStateInterface $form_state) {
    $element['remove_button']['#access'] = FALSE;
    return $element;
  }

}