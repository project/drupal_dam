<?php

namespace Drupal\drupal_dam\Plugin\EntityBrowser\Widget;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\drupal_dam\DAMBrowserWidgetTrait;
use Drupal\drupal_dam\Service\DAMJSONTalkService;
use Drupal\drupal_dam\Service\ResponseFormatterService;
use Drupal\entity_browser\WidgetBase;
use Drupal\entity_browser\WidgetValidationManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides dam browser widget.
 *
 *   allows multiple selections
 *
 * @EntityBrowserWidget(
 *   id = "dam_browser",
 *   label = @Translation("DAM Browser"),
 *   description = @Translation("Provides dam browser widget."),
 *   auto_select = FALSE
 * )
 */
class DAMBrowser extends WidgetBase {

  use DAMBrowserWidgetTrait;

  /**
   * @var \Drupal\drupal_dam\Service\DAMJSONTalkService
   */
  protected $jsonTalk;

  /**
   * @var \Drupal\drupal_dam\Service\ResponseFormatterService
   */
  protected $responseFormatter;

  /**
   * DAMBrowser constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\entity_browser\WidgetValidationManager $validation_manager
   * @param \Drupal\drupal_dam\Service\DAMJSONTalkService $json_talk
   * @param \Drupal\drupal_dam\Service\ResponseFormatterService $response_formatter
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EventDispatcherInterface $event_dispatcher, EntityTypeManagerInterface $entity_type_manager, WidgetValidationManager $validation_manager, DAMJSONTalkService $json_talk, ResponseFormatterService $response_formatter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $event_dispatcher, $entity_type_manager, $validation_manager);
    $this->jsonTalk = $json_talk;
    $this->responseFormatter = $response_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_browser.widget_validation'),
      $container->get('drupal_dam.jsontalk'),
      $container->get('drupal_dam.responseformatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'media_type' => NULL,
        'multiple' => FALSE,
        'recreate' => FALSE,
        'disable' => FALSE,
        'pagination' => 10,
        'submit_text' => $this->t('Save entity'),
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $provider = 'drupal_dam';
    $mediatypes = $this->entityTypeManager->getStorage('media_type')->loadMultiple();
    $options = [];
    foreach ($mediatypes as $mediatype) {
      $mediatypeConfig = $mediatype->getSource()->getPluginDefinition();
      if ($mediatypeConfig['provider'] == $provider) {
        $options[$mediatype->id()] = $mediatype->label();
      }
    }
    if (!empty($options)) {
      $form['media_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Media Type'),
        '#options' => $options,
        '#default_value' => $this->configuration['media_type']
      ];
    }
    $form['multiple'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow multiple selections'),
      '#default_value' => $this->configuration['multiple']
    ];
    $form['recreate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow recreation of already existing assets'),
      '#default_value' => $this->configuration['recreate']
    ];
    $form['disable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable the widget'),
      '#description' => $this->t('This is mainly helpful, if you intend to disable the widget on some condition via config override'),
      '#default_value' => $this->configuration['disable'] ?? FALSE
    ];
    $form['pagination'] = [
      '#type' => 'number',
      '#title' => $this->t('Pagination count'),
      '#min' => 1,
      '#max' => 50,
      '#step' => 1,
      '#default_value' => $this->configuration['pagination']
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $additional_widget_parameters) {
    $form = parent::getForm($original_form, $form_state, $additional_widget_parameters);
    // reset selected entities when rebuilding, cleanup selection when switching tabs in entity browser
    if ($form_state->isRebuilding()) {
      $user_input = $form_state->getUserInput();
      if (isset($user_input['dam_entities'])) {
        unset($user_input['dam_entities']);
      }
      if (isset($user_input['dam_entities_recreate'])) {
        unset($user_input['dam_entities_recreate']);
      }
      $form_state->setUserInput($user_input);
    }

    /* @var \Drupal\media\Entity\Media $media */
    $media = $this->entityTypeManager->getStorage('media')->create(['bundle' => $this->configuration['media_type']]);
    $mediaTypeSourceConfig = $media->getSource()->getConfiguration();
    $this->jsonTalk->setMediaBundle($media);
    if ($mediaTypeSourceConfig['dam_search']['enabled']) {
      $this->jsonTalk->setSearchAPIIndex();
      $this->jsonTalk->queryCondition('bundle', FALSE, $mediaTypeSourceConfig['dam_settings']['dam_mediabundle']);
      $this->jsonTalk->querySort('default-sort', $mediaTypeSourceConfig['dam_sort']['path'], $mediaTypeSourceConfig['dam_sort']['direction']);
    }
    $this->jsonTalk->queryPageLimit($this->configuration['pagination']);
    $response = $this->jsonTalk->doJSONRequest();
    $this->responseFormatter->setResponse($response, $this->jsonTalk->getMediaType());
    $form['dam_auswahl'] = $this->responseFormatter->getItemList($this->configuration['recreate']);

    $form['dam_entities'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'data-damuuid' => 'hidden-input'
      ]
    ];

    $form['dam_entities_recreate'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'data-damuuid' => 'hidden-input-recreate'
      ]
    ];
    $form['dam_entities']['#attached']['library'][] = 'drupal_dam/damgrid';
    $form['dam_entities']['#attached']['library'][] = 'drupal_dam/damchoose';
    $form['dam_entities']['#attached']['library'][] = 'drupal_dam/damsearchenter';
    $form['dam_entities']['#attached']['drupalSettings']['drupal_dam']['dam_browser_multiple'] = $this->configuration['multiple'];
    if ($mediaTypeSourceConfig['dam_search']['enabled']) {
      $form_state->setValue('attached_entity', $media);
      $form_state->setValue('dam_pagination', $this->configuration['pagination']);
      $form_state->setValue('use_existing', $this->configuration['recreate']);
      $form['search'] = $this->getSearchButton($mediaTypeSourceConfig['dam_search']['search_description']);
    }
    return $form;
  }


  /**
   * {@inheritdoc}
   */
  protected function prepareEntities(array $form, FormStateInterface $form_state) {
    $dam_entities = $form_state->getValue('dam_entities');
    // avoid creating entities when moving between different EB widget tabs
    if (empty($dam_entities) || $form_state->getTriggeringElement()['#type'] !== 'submit') {
      return [];
    }
    $dam_entities_recreate = $form_state->getValue('dam_entities_recreate');
    $dam_entities_array = explode(',', $dam_entities);
    $dam_entities_recreate_array = explode(',', $dam_entities_recreate);
    $entities = [];
    foreach ($dam_entities_array as $dam_entity) {
      /* @var \Drupal\media\Entity\Media $media */
      $media = $this->entityTypeManager->getStorage('media')
        ->create(['bundle' => $this->configuration['media_type']]);
      $sourceConfig = $media->getSource()->getConfiguration();
      $media_exists = $this->entityTypeManager->getStorage('media')
        ->loadByProperties([
          $sourceConfig['source_field'] => $dam_entity,
        ]);
      if (empty($media_exists) || in_array($dam_entity, $dam_entities_recreate_array)) {
        $media->set($sourceConfig['source_field'], $dam_entity);
        $media->save();
        $entities[] = $media;
      }
      else {
        $entities[] = array_shift($media_exists);
      }

    }
    $this->selectEntities($entities, $form_state);
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function access() {
    if (!empty($this->getConfiguration()['settings']['disable'])) {
      return AccessResult::forbidden();
    }
    return parent::access();
  }

}
