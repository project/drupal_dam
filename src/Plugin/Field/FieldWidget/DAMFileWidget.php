<?php

namespace Drupal\drupal_dam\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\drupal_dam\DAMBrowserWidgetTrait;
use Drupal\drupal_dam\Plugin\media\Source\DAMFile;
use Drupal\drupal_dam\Service\DAMJSONTalkService;
use Drupal\drupal_dam\Service\ResponseFormatterService;
use Drupal\media\Entity\MediaType;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Plugin implementation of the 'damfile_textfield' widget.
 *
 * This is for storing the UUID of the media entity from DAM
 *
 * @FieldWidget(
 *   id = "damfile_textfield",
 *   label = @Translation("DAM File UUID"),
 *   field_types = {
 *     "string",
 *   },
 * )
 */
class DAMFileWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  use DAMBrowserWidgetTrait;

  /* @var \Drupal\drupal_dam\Service\DAMJSONTalkService $jsonTalk */
  protected $jsonTalk;

  /**
   * @var \Drupal\drupal_dam\Service\ResponseFormatterService
   */
  protected $responseFormatter;


  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, DAMJSONTalkService $json_talk, ResponseFormatterService $response_formatter) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->jsonTalk = $json_talk;
    $this->responseFormatter = $response_formatter;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('drupal_dam.jsontalk'),
      $container->get('drupal_dam.responseformatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Used to extract search field value in ajax solr search
    $form_state->set('media_field_name', $items->getFieldDefinition()->getName());

    $jsonTalk = $this->jsonTalk;
    $jsonTalk->setMediaBundle($items->getEntity());
    if ($items->isEmpty()) {
      $mediaTypeSourceConfig = $items->getEntity()->getSource()->getConfiguration();
      $paginationCount = $mediaTypeSourceConfig['dam_pagination']['pagination_count'];
      if ($mediaTypeSourceConfig['dam_search']['enabled']) {
        $jsonTalk->setSearchAPIIndex();
        $jsonTalk->queryCondition('bundle', FALSE, $mediaTypeSourceConfig['dam_settings']['dam_mediabundle']);
        $jsonTalk->querySort('default-sort', $mediaTypeSourceConfig['dam_sort']['path'], $mediaTypeSourceConfig['dam_sort']['direction']);
      }
      $jsonTalk->queryPageLimit($paginationCount);
      $response = $jsonTalk->doJSONRequest();
      // an example URL: http://example.com/jsonapi/media/image?filter[name-filter][condition][path]=id&filter[name-filter][condition][operator]=NOT%20IN&filter[name-filter][condition][value][]=793bfb33-fc06-4f7a-b923-47f8c546a77a&filter[name-filter][condition][value][]=7610d0c2-c184-4240-9c30-992b433cbd15
      $this->responseFormatter->setResponse($response, $jsonTalk->getMediaType());
      $this->responseFormatter->setPaginationCount($paginationCount);
      $this->responseFormatter->setAllowRecreation($mediaTypeSourceConfig['dam_exists']['recreate']);
      $element['auswahl'] = $this->responseFormatter->getItemList();

      $element['value'] = $element + [
          '#type' => 'hidden',
          '#attributes' => [
            'data-damuuid' => 'hidden-input',
          ],
        ];
      $element['value']['#attached']['library'][] = 'drupal_dam/damgrid';
      $element['value']['#attached']['library'][] = 'drupal_dam/damchoose';
      $element['value']['#attached']['library'][] = 'drupal_dam/damsearchenter';
      if ($mediaTypeSourceConfig['dam_search']['enabled']) {
        $form_state->setValue('attached_entity', $items->getEntity());
        $form_state->setValue('dam_pagination', $paginationCount);
        $element['search'] = $this->getSearchButton($mediaTypeSourceConfig['dam_search']['search_description']);
      }


    }
    else {
      $response = $jsonTalk->doJSONRequest();

      $element['preview'] = [
        '#type' => 'markup',
        '#markup' => '<img src="' . $response->body->data->attributes->dam_thumbnail_url->url . '" />'
      ];
      $element['value'] = $element + [
          '#type' => 'hidden',
          '#value' => $items->value,
        ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $entity_type = $field_definition->getTargetEntityTypeId();
    if ($entity_type === 'media') {
      if ($target_bundle = $field_definition->getTargetBundle()) {
        $media_type = MediaType::load($target_bundle);
        return (!empty($media_type) && $media_type->getSource() instanceof DAMFile);
      }
    }
    return FALSE;
  }

  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    return parent::massageFormValues($values, $form, $form_state); // TODO: Change the autogenerated stub
  }

}
