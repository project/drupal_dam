<?php

namespace Drupal\drupal_dam\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\drupal_dam\LocalCopyWidgetTrait;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;
use Drupal\drupal_dam\Plugin\media\Source\DAMFile;
use Drupal\media\Entity\MediaType;

/**
 * Plugin implementation of the 'dam_localcopy_file' widget.
 *
 * derives basically from file_generic widget but hides file upload during
 * creation and file remove while editing. this is to ensure that the
 * referenced file (image) is handled solely from DAM Remote
 *
 * @FieldWidget(
 *   id = "dam_localcopy_file",
 *   label = @Translation("DAM File widget for local copy file"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class DAMLocalCopyFileWidget extends FileWidget {

  use LocalCopyWidgetTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(\Drupal\Core\Field\FieldItemListInterface $items, $delta, array $element, array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element = $this->localCopyFormElement($items, $delta, $element, $form, $form_state);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $entity_type = $field_definition->getTargetEntityTypeId();
    if ($entity_type === 'media') {
      if ($target_bundle = $field_definition->getTargetBundle()) {
        $media_type = MediaType::load($target_bundle);
        return (!empty($media_type) && $media_type->getSource() instanceof DAMFile);
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    /* @var \Drupal\media\MediaForm $formObject */
    $formObject = $form_state->getFormObject();
    if ($formObject->getOperation() == 'add') {
      return $values;
    }
    else {
      return parent::massageFormValues($values, $form, $form_state);
    }
  }

}
