<?php

namespace Drupal\drupal_dam\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\drupal_dam\LocalCopyWidgetTrait;
use Drupal\drupal_dam\Plugin\media\Source\DAMImage;
use Drupal\image\Plugin\Field\FieldWidget\ImageWidget;
use Drupal\media\Entity\MediaType;

/**
 * Plugin implementation of the 'dam_localcopy_image' widget.
 *
 * derives basically from image_image widget but hides file upload during
 * creation and file remove while editing. this is to ensure that the
 * referenced file (image) is handled solely from DAM Remote
 * offers a widget setting that let users choose to display alt and title
 * attribute as read-only fields
 *
 * @FieldWidget(
 *   id = "dam_localcopy_image",
 *   label = @Translation("DAM Image widget for local copy image"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class DAMLocalCopyImageWidget extends ImageWidget {

  use LocalCopyWidgetTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'read_only' => [
          'alt' => FALSE,
          'title' => FALSE,
        ]
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['read_only'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Read-only attributes'),
      '#tree' => TRUE,
    ];
    $element['read_only']['alt'] = [
      '#title' => $this->t('Make Alt attribute read-only'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('read_only')['alt'],
    ];
    $element['read_only']['title'] = [
      '#title' => $this->t('Make Title attribute read-only'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('read_only')['title'],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $read_only_alt = $this->getSetting('read_only')['alt'];
    if (!empty($read_only_alt)) {
      $summary[] = $this->t('Alt attribute is read-only');
    }
    $read_only_title = $this->getSetting('read_only')['title'];
    if (!empty($read_only_title)) {
      $summary[] = $this->t('Title attribute is read-only');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(\Drupal\Core\Field\FieldItemListInterface $items, $delta, array $element, array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element = $this->localCopyFormElement($items, $delta, $element, $form, $form_state);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function afterBuild(array $element, FormStateInterface $form_state) {
    $entity_form = $form_state->getFormObject();
    if ($entity_form instanceof EntityFormInterface) {
      $form_display = $entity_form->getFormDisplay($form_state);
      if ($form_display instanceof EntityFormDisplayInterface) {
        $component = $form_display->getComponent($element['#field_name']);
        if (!empty($component['settings']['read_only'])) {
          $read_only_settings = $component['settings']['read_only'];
          foreach ($read_only_settings as $attribute => $enabled) {
            if (!empty($element[0][$attribute]) && $enabled) {
              $value = $element[0][$attribute]['#default_value'];
              $name = $element[0][$attribute]['#name'];
              $title = $element[0][$attribute]['#title'];
              $description = $element[0][$attribute]['#description'];
              $weight = $element[0][$attribute]['#weight'];
              $element[0][$attribute] = [
                '#type' => 'container',
                '#weight' => $weight ?? 0,
                'value' => [
                  '#type' => 'hidden',
                  '#value' => $value,
                  '#name' => $name
                ],
                'description' => [
                  '#type' => 'item',
                  '#title' => $title,
                  '#markup' => $value,
                  '#description' => $description,
                  '#description_display' => 'after',
                ]
              ];
            }
          }
        }
      }
    }
    return parent::afterBuild($element, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $entity_type = $field_definition->getTargetEntityTypeId();
    if ($entity_type === 'media') {
      if ($target_bundle = $field_definition->getTargetBundle()) {
        $media_type = MediaType::load($target_bundle);
        return (!empty($media_type) && $media_type->getSource() instanceof DAMImage);
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    /* @var \Drupal\media\MediaForm $formObject */
    $formObject = $form_state->getFormObject();
    if ($formObject->getOperation() == 'add') {
      return $values;
    }
    else {
      return parent::massageFormValues($values, $form, $form_state);
    }
  }


}
