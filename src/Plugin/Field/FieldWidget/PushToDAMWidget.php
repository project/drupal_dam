<?php

namespace Drupal\drupal_dam\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'push_to_dam_default' widget.
 *
 * @FieldWidget(
 *   id = "push_to_dam_default",
 *   label = @Translation("Push to DAM"),
 *   field_types = {
 *     "map"
 *   }
 * )
 */
class PushToDAMWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Getting value to check if this item has already been pushed to DAM
    $value = $items->getValue();
    $alreadyPushed = isset($value[0]['pushed']) ? explode(';', $value[0]['pushed']) : [];

    /* @var \Drupal\media\Entity\Media $media */
    $media = $items->getEntity();
    /* @var \Drupal\media\Entity\MediaType $mediaType */
    $mediaType = $this->entityTypeManager
      ->getStorage('media_type')
      ->load($media->bundle());
    if ($mediaType->getSource() instanceof \Drupal\drupal_dam\Plugin\media\Source\DAMFile === FALSE) {
      $damSettings = $mediaType->getThirdPartySettings('drupal_dam');
      $options = [];
      if (!empty($damSettings['copydestination'])) {
        foreach ($damSettings['copydestination'] as $destination => $enabled) {
          if ($destination === $enabled && !in_array($destination, $alreadyPushed)) {
            $copyType = \Drupal::entityTypeManager()
              ->getStorage('media_type')
              ->load($destination);
            $options[$destination] = $copyType->label();
          }
        }
        if (!empty($options)) {
          $element['push_to_dam'] = [
            '#type' => 'checkboxes',
            '#options' => $options,
            '#title' => t('Push to DAM'),
          ];
        }
        $element['pushed'] = [
          '#type' => 'hidden',
          '#value' => $value[0]['pushed'] ?? FALSE
        ];
      }
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getFieldStorageDefinition()->getName() == 'pushed_to_dam';
  }
}