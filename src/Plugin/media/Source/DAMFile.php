<?php

namespace Drupal\drupal_dam\Plugin\media\Source;

use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\media\Entity\MediaType;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;

/**
 * Provides a media source plugin for files from DAM.
 *
 *
 * @MediaSource(
 *   id = "dam_file",
 *   label = @Translation("DAM File"),
 *   description = @Translation("Use File from DAM."),
 *   allowed_field_types = {"string"},
 *   default_thumbnail_filename = "generic.png",
 * )
 */
class DAMFile extends MediaSourceBase {

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    // TODO: Implement getMetadataAttributes() method to use
  }

  /**
   * {@inheritdoc}
   *
   * set sane source field and localcopy field widgets for form display
   */
  public function prepareFormDisplay(MediaTypeInterface $type, EntityFormDisplayInterface $display) {
    parent::prepareFormDisplay($type, $display);
    $source_field = $this->getSourceFieldDefinition($type)->getName();

    $display->setComponent($source_field, [
      'type' => 'damfile_textfield',
      'weight' => $display->getComponent($source_field)['weight'],
    ]);
    $localcopy_field = $this->getLocalCopyFieldDefinition($type)->getName();
    $display->setComponent($localcopy_field, [
      'type' => str_replace('dam', 'dam_localcopy', $this->getPluginId()),
      'weight' => $display->getComponent($localcopy_field)['weight'] ?? 0,
    ]);
    // ToDo: Think about how to handle name
    $display->removeComponent('name');
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $meta_data = parent::getMetadata($media, $attribute_name);
    if ($attribute_name == 'thumbnail_uri') {
      if (!empty($this->configuration['thumbnail_file']) && $this->configuration['thumbnail_file'] != '_none') {
        return $this->configFactory->get('media.settings')
          ->get('icon_base_uri') . '/' . $this->configuration['thumbnail_file'];
      }
    }
    return $meta_data;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'dam_settings' => [
          'dam_address' => '',
          'dam_address_public' => FALSE,
          'dam_mediabundle' => '',
        ],
        'dam_authentication' => [
          'auth_enabled' => ''
        ],
        'dam_search' => [
          'enabled' => FALSE,
          'search_index' => '',
          'search_description' => '',
        ],
        'dam_exists' => [
          'recreate' => FALSE
        ],
        'dam_pagination' => [
          'pagination_count' => 8
        ],
        'dam_sort' => [
          'path' => 'created',
          'direction' => 'DESC',
          'sort_options' => [],
        ],
        'dam_display' => [
          'item_label' => $this->getPluginId() === 'dam_file',
          'item_file_size' => FALSE,
          'item_file_mime' => FALSE,
          'page_counter' => TRUE,
          'pager_size' => 0,
        ],
        'localcopy_field' => '',
        'thumbnail_file' => FALSE,
      ] + parent::defaultConfiguration();
  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['dam_settings'] = [
      '#type' => 'container'
    ];
    $form['dam_settings']['dam_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('DAM address'),
      '#default_value' => $this->configuration['dam_settings']['dam_address'],
      '#description' => $this->t('Enter address of DAM'),
      '#required' => TRUE,
    ];
    $form['dam_settings']['dam_address_public'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public DAM address'),
      '#default_value' => $this->configuration['dam_settings']['dam_address_public'],
      '#description' => $this->t('Public accessible address of DAM (only necessary if real DAM address is not publicly accessible)'),
    ];
    $form['dam_settings']['dam_mediabundle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('DAM media bundle'),
      '#default_value' => $this->configuration['dam_settings']['dam_mediabundle'],
      '#description' => $this->t('Machine name of the media bundle from DAM to use'),
      '#required' => TRUE,
    ];

    // New authentication feature - needed for contacting different hosts with authentication
    $form['dam_authentication'] = [
      '#type' => 'container'
    ];
    $auth_options = [
      '' => $this->t('No authentication')
    ];
    $auth_configs = $this->entityTypeManager->getStorage('drupal_dam_auth')->loadMultiple();
    $auth_config_prefix = 'drupal_dam.drupal_dam_auth.';
    foreach ($auth_configs as $auth_config_id => $auth_config) {
      $auth_options[$auth_config_prefix.$auth_config_id] = $auth_config->label();
    }
    $form['dam_authentication']['auth_enabled'] = [
      '#type' => 'select',
      '#title' => $this->t('Use authentication'),
      '#options' => $auth_options,
      '#default_value' => $this->configuration['dam_authentication']['auth_enabled']
    ];

    if (empty($this->configuration['localcopy_field'])) {
      $form['localcopy_field'] = [
        '#type' => 'textfield',
        '#default_value' => $this->configuration['localcopy_field'],
        '#title' => $this->t('Reuse existing localcopy field'),
        '#description' => $this->t('Attention: Use only if you know what you are doing. You can leave this blank in most cases.')
      ];
    }
    else {
      $form['localcopy_field'] = [
        '#type' => 'hidden',
        '#default_value' => $this->configuration['localcopy_field']
      ];
    }

    if ($this->configuration['dam_settings']['dam_address']) {
      $form['dam_search'] = [
        '#type' => 'details',
        '#title' => $this->t('Search configuration'),
        '#open' => TRUE,
      ];
      $form['dam_search']['enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable Search (Search API needed on host)'),
        '#default_value' => $this->configuration['dam_search']['enabled']
      ];
      $form['dam_search']['search_index'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Search API index machine name on host'),
        '#default_value' => $this->configuration['dam_search']['search_index'],
        '#states' => [
          'visible' => [
            ':input[name="source_configuration[dam_search][enabled]"]' => ['checked' => TRUE]
          ]
        ]
      ];
      $form['dam_search']['search_description'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Search button description'),
        '#default_value' => $this->configuration['dam_search']['search_description'],
        '#states' => [
          'visible' => [
            ':input[name="source_configuration[dam_search][enabled]"]' => ['checked' => TRUE]
          ]
        ]
      ];
    }

    $form['dam_sort'] = [
      '#type' => 'details',
      '#title' => $this->t('Sorting configuration'),
      '#open' => TRUE,
    ];
    $form['dam_sort']['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Path/field to sort DAM results on'),
      '#description' => $this->t('needs to exist in search index as a field too if using Search'),
      '#default_value' => $this->configuration['dam_sort']['path'],
    ];
    $form['dam_sort']['direction'] = [
      '#type' => 'select',
      '#title' => $this->t('Default direction of sort'),
      '#options' => [
        'ASC' => $this->t('Ascending'),
        'DESC' => $this->t('Descending')
      ],
      '#default_value' => $this->configuration['dam_sort']['direction'],
    ];

    $form['dam_sort']['sort_options'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Sort options'),
      '#default_value' => $this->allowedValuesString($this->configuration['dam_sort']['sort_options']),
      '#rows' => 10,
      '#element_validate' => [[static::class, 'validateAllowedValues']],
      '#description' => $this->t('One sort option per line, style option|label possible for custom label, sort option can be a field/property of this media entity, needs to exist in search index as a field too if using Search.')
    ];

    $form['dam_exists'] = [
      '#type' => 'container'
    ];
    $form['dam_exists']['recreate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow to recreate already existing assets'),
      '#description' => $this->t('Can be overriden for entity browser widgets'),
      '#default_value' => $this->configuration['dam_exists']['recreate']
    ];
    $form['dam_display'] = [
      '#type' => 'details',
      '#title' => $this->t('DAM selection widget display configuration'),
      '#open' => TRUE,
    ];
    $form['dam_display']['item_label'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Output item label'),
      '#default_value' => $this->configuration['dam_display']['item_label']
    ];
    $form['dam_display']['item_file_size'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Output item file size'),
      '#default_value' => $this->configuration['dam_display']['item_file_size']
    ];
    $form['dam_display']['item_file_mime'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Output item file mime type'),
      '#default_value' => $this->configuration['dam_display']['item_file_mime']
    ];
    $form['dam_display']['page_counter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show items/page counter'),
      '#default_value' => $this->configuration['dam_display']['page_counter']
    ];
    $form['dam_display']['pager_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Size of pager'),
      '#description' => $this->t('Number of pager elements to the left an right, use 0 to disable numbered pager.'),
      '#min' => 0,
      '#step' => 1,
      '#default_value' => $this->configuration['dam_display']['pager_size'],
    ];
    $form['dam_pagination'] = [
      '#type' => 'container'
    ];
    $form['dam_pagination']['pagination_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Pagination count'),
      '#min' => 1,
      '#max' => 50,
      '#step' => 1,
      '#default_value' => $this->configuration['dam_pagination']['pagination_count'],
      '#description' => $this->t('Can be overriden for entity browser widgets')
    ];
    if (!($this instanceof DAMImage)) {
      $form['thumbnail_file'] = [
        '#type' => 'select',
        '#title' => $this->t('Different thumbnail file'),
        '#options' => [
          '_none' => $this->t('Default'),
          'audio.png' => $this->t('Audio'),
          'video.png' => $this->t('Video')
        ],
        '#default_value' => $this->configuration['thumbnail_file'],
      ];
    }

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    /* @var \Drupal\drupal_dam\Service\DAMJSONTalkService $jsonTalk */
    $jsonTalk = \Drupal::service('drupal_dam.jsontalk');
    $damAuth = $form_state->getValue('dam_authentication');
    $damSettings = $form_state->getValue('dam_settings');
    // URL with trailing slash works for check but makes problems elsewhere
    $damSettings['dam_address'] = rtrim($damSettings['dam_address'], '/');
    $form_state->setValue('dam_settings', $damSettings);
    $dam_check = $this->check_dam_config_override($form_state->getFormObject()->getEntity(), $damSettings);
    $status = $jsonTalk->checkJSON($dam_check['dam_address'], $dam_check['dam_mediabundle'], $damAuth);
    if ($status['error']) {
      $form_state->setErrorByName('dam_settings', $status['message']);
    }
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (empty($this->configuration['localcopy_field'])) {
      $field_storage = $this->createLocalCopyFieldStorage();
      $field_storage->save();
      $this->configuration['localcopy_field'] = $field_storage->getName();
    }
  }

  /**
   * @return \Drupal\Core\Entity\EntityInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createLocalCopyFieldStorage() {
    return $this->entityTypeManager
      ->getStorage('field_storage_config')
      ->create([
        'entity_type' => 'media',
        'field_name' => $this->getLocalCopyFieldName(),
        'type' => 'file',
      ]);
  }

  /**
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getLocalCopyFieldName() {
    // Some media sources are using a deriver, so their plugin IDs may contain
    // a separator (usually ':') which is not allowed in field names.
    $base_id = 'field_damcopy_' . str_replace(static::DERIVATIVE_SEPARATOR, '_', $this->getPluginId());
    $tries = 0;
    $storage = $this->entityTypeManager->getStorage('field_storage_config');

    // Iterate at least once, until no field with the generated ID is found.
    do {
      $id = $base_id;
      // If we've tried before, increment and append the suffix.
      if ($tries) {
        $id .= '_' . $tries;
      }
      $field = $storage->load('media.' . $id);
      $tries++;
    } while ($field);

    return $id;
  }

  /**
   * @param \Drupal\media\MediaTypeInterface $type
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface|null
   */
  public function getLocalCopyFieldDefinition(MediaTypeInterface $type) {
    // Nothing to do if no localcopy field is configured yet.
    $field = $this->configuration['localcopy_field'];
    if ($field) {
      // Even if we do know the name of the source field, there is no
      // guarantee that it already exists.
      $fields = $this->entityFieldManager->getFieldDefinitions('media', $type->id());
      return isset($fields[$field]) ? $fields[$field] : NULL;
    }
    return NULL;
  }

  /**
   * @param \Drupal\media\MediaTypeInterface $type
   *
   * @return \Drupal\Core\Entity\EntityInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createLocalCopyField(MediaTypeInterface $type) {
    $storage = $this->getLocalCopyFieldStorage() ?: $this->createLocalCopyFieldStorage();
    return $this->entityTypeManager
      ->getStorage('field_config')
      ->create([
        'field_storage' => $storage,
        'bundle' => $type->id(),
        'label' => $this->t('Local copy field'),
        'required' => FALSE,
      ]);
  }

  /**
   * @return \Drupal\Core\Field\FieldStorageDefinitionInterface|null
   */
  protected function getLocalCopyFieldStorage() {
    // Nothing to do if no source field is configured yet.
    $field = $this->configuration['localcopy_field'];
    if ($field) {
      // Even if we do know the name of the source field, there's no
      // guarantee that it exists.
      $fields = $this->entityFieldManager->getFieldStorageDefinitions('media');
      return isset($fields[$field]) ? $fields[$field] : NULL;
    }
    return NULL;
  }

  /**
   * redirect to configuration form again after adding a new media type
   * for advanced configuration
   * see drupal_dam_form_media_type_add_form_alter()
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public static function add_source_redirect(array &$form, FormStateInterface $form_state) {
    $source = $form_state->getValue('source');
    // ToDo: Can we get this more generally? Something like instanceOf?
    if (!empty($source) && ($source == 'dam_file' || $source == 'dam_image')) {
      $form_state->setRedirect('entity.media_type.edit_form', ['media_type' => $form_state->getValue('id')]);
      \Drupal::messenger()->addMessage(t('You can now configure advanced settings for the new media type.'));
    }
  }

  /**
   * get config overrides of DAM settings
   *
   * @param \Drupal\media\Entity\MediaType $media_type
   * @param array $dam_settings
   *
   * @return array
   *   dam settings with config overrides
   */
  protected function check_dam_config_override(MediaType $media_type, array $dam_settings) {
    $dam_check = [
      'dam_address' => $dam_settings['dam_address'],
      'dam_mediabundle' => $dam_settings['dam_mediabundle']
    ];

    $overriden = \Drupal::config('media.type.'.$media_type->id())->get('source_configuration');
    $original = \Drupal::config('media.type.'.$media_type->id())->getOriginal('source_configuration', FALSE);
    if (isset($overriden['dam_settings']['dam_address'])) {
      if ($overriden['dam_settings']['dam_address'] != $original['dam_settings']['dam_address']) {
        $dam_check['dam_address'] = $overriden['dam_settings']['dam_address'];
        $this->messenger()->addWarning(
          t('DAM address is overriden by config to %dam_address. Changes you make on DAM address here might not have any impact.',
            ['%dam_address' => $dam_check['dam_address']])
        );
      }
    }
    if (isset($overriden['dam_settings']['dam_mediabundle'])) {
      if ($overriden['dam_settings']['dam_mediabundle'] != $original['dam_settings']['dam_mediabundle']) {
        $dam_check['dam_mediabundle'] = $overriden['dam_settings']['dam_mediabundle'];
        $this->messenger()->addWarning(
          t('DAM media bundle is overriden by config to %bundle. Changes you make on DAM media bundle here might not have any impact.',
            ['%bundle' => $dam_check['dam_mediabundle']])
        );
      }
    }
    return $dam_check;
  }

  /**
   * @see Drupal\options\Plugin\Field\FieldType\ListItemBase
   *
   * Generates a string representation of an array of 'allowed values'.
   *
   * This string format is suitable for edition in a textarea.
   *
   * @param array $values
   *   An array of values, where array keys are values and array values are
   *   labels.
   *
   * @return string
   *   The string representation of the $values array:
   *    - Values are separated by a carriage return.
   *    - Each value is in the format "value|label" or "value".
   */
  protected function allowedValuesString($values) {
    $lines = [];
    foreach ($values as $key => $value) {
      $lines[] = "$key|$value";
    }
    return implode("\n", $lines);
  }

  /**
   * @see Drupal\options\Plugin\Field\FieldType\ListItemBase
   *
   * put values form textarea input line by line into sequence config
   *
   * @param $element
   * @param $form_state
   *
   * @return void
   */
  public static function validateAllowedValues($element, $form_state) {
    $values = [];

    $list = explode("\n", $element['#value']);
    $list = array_map('trim', $list);
    $list = array_filter($list, 'strlen');

    $generated_keys = $explicit_keys = FALSE;
    foreach ($list as $position => $text) {
      // Check for an explicit key.
      $matches = [];
      if (preg_match('/(.*)\|(.*)/', $text, $matches)) {
        // Trim key and value to avoid unwanted spaces issues.
        $key = trim($matches[1]);
        $value = trim($matches[2]);
      }
      // Otherwise see if we can generate a key from the position.
      else {
        $key = $value = $text;
      }

      $values[$key] = $value;
    }

    $form_state->setValueForElement($element, $values);
  }

}
