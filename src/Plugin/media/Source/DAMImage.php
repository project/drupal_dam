<?php

namespace Drupal\drupal_dam\Plugin\media\Source;

use Drupal\Core\File\FileSystemInterface;
use Drupal\drupal_dam\Service\DAMJSONTalkService;
use Drupal\field\Entity\FieldConfig;
use Drupal\media\MediaInterface;

/**
 * Provides a media source plugin for files from DAM.
 *
 *
 * @MediaSource(
 *   id = "dam_image",
 *   label = @Translation("DAM Image"),
 *   description = @Translation("Use Image from DAM."),
 *   allowed_field_types = {"string"},
 *   default_thumbnail_filename = "no-thumbnail.png",
 *   thumbnail_alt_metadata_attribute = "thumbnail_alt_value"
 * )
 */
class DAMImage extends DAMFile {

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $mediaSourceConfig = $media->getSource()->getConfiguration();
    switch ($attribute_name) {
      case 'thumbnail_uri':
        /* @var \Drupal\drupal_dam\Service\DAMFileRetrieveService $fileRetriever */
        $fileRetriever = \Drupal::service('drupal_dam.fileretrieve');
        $localfile = $fileRetriever->retrieveURL($media);
        return $localfile->getFileUri();
      case 'thumbnail_alt_value':
        /* @var \Drupal\drupal_dam\Service\DAMJSONTalkService $jsonTalk */
        $jsonTalk = \Drupal::service('drupal_dam.jsontalk');
        $jsonTalk->setMediaBundle($media);
        $response = $jsonTalk->doJSONRequest();
        return $response->body->data->relationships->thumbnail->data->meta->alt;
      case 'default_name':
        return 'Remote UUID: '. $media->{$mediaSourceConfig['source_field']}->value;
    }

    return parent::getMetadata($media, $attribute_name);
  }

  /**
   * {@inheritdoc}
   */
  protected function createLocalCopyFieldStorage() {
    return $this->entityTypeManager
      ->getStorage('field_storage_config')
      ->create([
        'entity_type' => 'media',
        'field_name' => $this->getLocalCopyFieldName(),
        'type' => 'image',
      ]);
  }
}
