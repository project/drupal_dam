<?php

namespace Drupal\drupal_dam\Service;

use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\file\FileRepositoryInterface;
use Drupal\media\MediaInterface;

class DAMFileRetrieveService {

  /**
   * @var \Drupal\drupal_dam\Service\DAMJSONTalkService
   */
  protected $jsonTalk;

  /**
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystemService;

  /**
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;
  /**
   * @var FieldConfig
   */
  protected $localCopyFieldConfig;

  /**
   * @var \Drupal\file\FileInterface
   */
  protected $localFile;

  /**
   * @var array
   * plugin configuration of media source plugin
   */
  protected $mediaSourceConfig;

  public function __construct(DAMJSONTalkService $json_talk, FileSystem $file_system_service, FileRepositoryInterface $file_repository) {
    $this->jsonTalk = $json_talk;
    $this->fileSystemService = $file_system_service;
    $this->fileRepository = $file_repository;
  }

  /**
   * retrieve a media file from DAM and store it, return file entity
   *
   * @param \Drupal\media\MediaInterface $media
   *
   * @return bool|\Drupal\file\FileInterface|mixed
   */
  public function retrieveURL(MediaInterface $media) {
    $this->jsonTalk->setMediaBundle($media);
    $this->mediaSourceConfig = $media->getSource()->getConfiguration();
    $this->localCopyFieldConfig = $this->getLocalCopyFieldConfig($media);
    $uri_scheme = $this->localCopyFieldConfig->getFieldStorageDefinition()->getSetting('uri_scheme') ?? 'public';
    $localCopyFileDirectory = $uri_scheme . '://'.\Drupal::token()->replace($this->localCopyFieldConfig->getSetting('file_directory'));
    $this->fileSystemService->prepareDirectory($localCopyFileDirectory, FileSystemInterface::CREATE_DIRECTORY);
    $this->jsonTalk->addJSONDestination($this->jsonTalk->getDAMMediaSourceField());
    $response = $this->jsonTalk->doJSONRequest();
    $remote_file = $response->body->data->attributes->uri->url;
    $data = (string) \Drupal::httpClient()->get($this->mediaSourceConfig['dam_settings']['dam_address'] . $remote_file)->getBody();
    $this->localFile = $this->fileRepository->writeData($data, $localCopyFileDirectory . '/' . basename($remote_file), FileSystemInterface::EXISTS_RENAME);
    return $this->localFile;
  }

  /**
   * retrieve a normal file from DAM
   *
   * @param $file_response
   * @param $field_name
   * @param \Drupal\media\MediaInterface $media
   *
   * @return false|mixed
   */
  public function retrieveFileURL($file_response, $field_name, MediaInterface $media) {
    $this->mediaSourceConfig = $media->getSource()->getConfiguration();
    // Skip for the media file field
    if ($field_name != $this->mediaSourceConfig['localcopy_field']) {
      $local_field_config = FieldConfig::loadByName('media', $media->bundle(), $field_name);
      $uri_scheme = $local_field_config->getFieldStorageDefinition()
          ->getSetting('uri_scheme') ?? 'public';
      $local_directory = $uri_scheme . '://' . \Drupal::token()
          ->replace($local_field_config->getSetting('file_directory'));
      $this->fileSystemService->prepareDirectory($local_directory, FileSystemInterface::CREATE_DIRECTORY);
      $remote_file = $file_response->body->data->attributes->uri->url;
      $data = (string) \Drupal::httpClient()->get($this->mediaSourceConfig['dam_settings']['dam_address'] . $remote_file)->getBody();
      return $this->fileRepository->writeData($data, $local_directory . '/' . basename($remote_file), FileSystemInterface::EXISTS_RENAME);
    }
    else {
      return FALSE;
    }
  }

  /**
   * return field config of localcopy field
   *
   * @param \Drupal\media\MediaInterface $media
   *
   * @return \Drupal\field\Entity\FieldConfig
   */
  protected function getLocalCopyFieldConfig(MediaInterface $media) {
    return FieldConfig::loadByName('media', $media->bundle(), $this->mediaSourceConfig['localcopy_field']);
  }



}
