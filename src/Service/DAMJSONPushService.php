<?php

namespace Drupal\drupal_dam\Service;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\taxonomy\Entity\Term;
use Psr\Log\LoggerInterface;

class DAMJSONPushService {

  use StringTranslationTrait;

  /**
   * http headers
   *
   * @var array|string[]
   */
  protected $headers = [
    'Content-Type' => 'application/vnd.api+json',
    'Accept' => 'application/vnd.api+json'
  ];

  /**
   * @var \Drupal\media\Entity\MediaType
   */
  protected $destinationMediaType;

  /**
   * @var \Drupal\media\Entity\MediaType
   */
  protected $sourceMediaType;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\drupal_dam\Service\DAMOAuthService
   */
  protected $damOAuthService;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\drupal_dam\Service\JSONRequestService
   */
  protected $jsonRequest;

  /**
   * @var array
   *   destination source configuration
   *
   */
  protected $destinationSourceConfig;

  /**
   * @var array
   *   source source configuration
   */
  protected $sourceSourceConfig;

  /**
   * @var \Drupal\file\Entity\File
   */
  protected $pushFile;

  /**
   * fields that can be pushed to host (reading from host entity form display
   * configuration
   *
   * @var array
   *
   */
  protected $pushFieldConfig;

  /**
   * attributes to be pushed to host
   *
   * name is required
   *
   * @var string[]
   */
  protected $pushAttributes = [
    'name' => 'Pushed Asset'
  ];

  /**
   * @var array
   */
  protected $pushRelationships = [];

  /**
   * source field value getting from form state
   *
   * @var \Drupal\file\Plugin\Field\FieldType\FileFieldItemList
   */
  protected $sourceFieldValue;

  /**
   * @var string
   *   source field of destination bundle
   */
  protected $DAMMediaSourceField;

  /**
   * DAMJSONPushService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\drupal_dam\Service\DAMOAuthService $dam_oauth_service
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   * @param \Psr\Log\LoggerInterface $logger
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * @param \Drupal\drupal_dam\Service\JSONRequestService $json_request
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DAMOAuthService $dam_oauth_service, EntityFieldManagerInterface $entity_field_manager, LoggerInterface $logger, MessengerInterface $messenger, JSONRequestService $json_request) {
    $this->entityTypeManager = $entity_type_manager;
    $this->damOAuthService = $dam_oauth_service;
    $this->entityFieldManager = $entity_field_manager;
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->jsonRequest = $json_request;
    if ($this->damOAuthService->auth_enabled()) {
      $this->headers = array_merge($this->headers, $this->getAuthHeader());
    }
  }

  /**
   * preparing the push to DAM
   *
   * @param $destination
   *   destination media type id
   * @param $source
   *   source media type id
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function preparePush($destination, $source, EntityInterface $entity) {
    $this->destinationMediaType = $this->entityTypeManager->getStorage('media_type')->load($destination);
    $this->destinationSourceConfig = $this->destinationMediaType->getSource()->getConfiguration();
    $this->sourceMediaType = $this->entityTypeManager->getStorage('media_type')->load($source);
    $this->sourceSourceConfig = $this->sourceMediaType->getSource()->getConfiguration();
    $sourceField = $this->sourceSourceConfig['source_field'];
    $this->sourceFieldValue = $entity->{$sourceField};
    $this->pushFile = $this->entityTypeManager->getStorage('file')->load($entity->{$sourceField}->target_id);
    $this->setDAMMediaSourceField();
    $this->setSyncDisplayConfiguration();
    \Drupal::moduleHandler()->alter('drupal_dam_push_entity', $entity);
  }

  /**
   * push media to DAM host in two steps
   *
   * first step is to push the file
   * second step is to create media entity and attach the file to this entity
   *
   * This is inspired from https://www.drupal.org/node/3024331
   *
   * @return bool
   */
  public function pushToHost() {
    // First step - pushing the file
    if (!empty($this->destinationSourceConfig['dam_authentication']['auth_enabled'])) {
      $this->damOAuthService->setAuthConfig($this->destinationSourceConfig['dam_authentication']['auth_enabled']);
    }
    $this->jsonRequest->setHTTPMethod('POST');
    $this->jsonRequest->setRequestURI($this->getDestinationURL() . '/jsonapi/media/' . $this->getDestinationBundle() . '/' . $this->getDAMMediaSourceField());
    $this->jsonRequest->addHeaders($this->stepOneHeaders());
    $this->jsonRequest->setRequestBody($this->getPushFile());
    $this->jsonRequest->doRequest();
    $pushStepOne = $this->jsonRequest->getResponseBody();
    $responseCode = $this->jsonRequest->getResponseCode();
    if ($responseCode == '201') {
      $destinationFileUUID = $pushStepOne->data->id;
    }
    else {
      $this->logger->error(
        'Error pushing asset to host (file part). HTTP %code %message',
        [
          '%code' => $responseCode,
          '%message' => $this->jsonRequest->getErrorMessage(),
        ]
      );
      $this->messenger->addError($this->t('Error creating asset (file part) of type %type on host', ['%type' => $this->getDestinationBundle()]));
    }

    // Second step creating media entity and attaching the file
    if (!empty($destinationFileUUID)) {
      $this->jsonRequest->resetOptions();
      $this->jsonRequest->setHTTPMethod('POST');
      $this->jsonRequest->setRequestURI($this->getDestinationURL() . '/jsonapi/media/' . $this->getDestinationBundle());
      $this->jsonRequest->setRequestBody(json_encode($this->requestBodyStepTwo($destinationFileUUID)));
      $this->jsonRequest->addHeaders($this->headers);
      $this->jsonRequest->doRequest();
      $pushStepTwo = $this->jsonRequest->getResponseBody();
      $responseCode = $this->jsonRequest->getResponseCode();
      if ($responseCode == '201') {
        $this->logger->notice('Succesfully pushed asset to host - Host UUID: %uuid', ['%uuid' => $pushStepTwo->data->id]);
        $this->messenger->addMessage($this->t('Succesfully created asset of type %type on host', ['%type' => $this->getDestinationBundle()]));
        return TRUE;
      }
      else {
        $this->logger->error(
          'Error pushing asset to host. HTTP %code %message',
          [
            '%code' => $responseCode,
            '%message' => $this->jsonRequest->getErrorMessage(),
          ]
        );
        $this->messenger->addError($this->t('Error creating asset of type %type on host', ['%type' => $this->getDestinationBundle()]));
      }
    }
    return FALSE;
  }

  /**
   * set the push attributes (fields) by checking local fields against fields
   * from push config on host
   *
   * see @setSyncDisplayConfiguration()
   *
   * @param \Drupal\media\Entity\Media $media
   */
  public function setPushAttributes(Media $media) {
    $relationship_types = ['entity_reference', 'file'];
    $local_fields = $media->getFields();
    foreach ($this->pushFieldConfig as $field => $config) {
      if (isset($local_fields[$field])) {
        if (!in_array($local_fields[$field]->getFieldDefinition()->getType(), $relationship_types)) {
          if (!$media->{$field}->isEmpty()) {
            $this->pushAttributes[$field] = [];
            foreach ($media->{$field} as $item) {
              $value = $item->getValue();
              $valueColumns = [];
              foreach ($value as $column => $val) {
                $valueColumns[$column] = $val;
              }
              if ($local_fields[$field]->getFieldDefinition()->getFieldStorageDefinition()->isMultiple()) {
                $this->pushAttributes[$field][] = $valueColumns;
              }
              else {
                $this->pushAttributes[$field] = $valueColumns;
              }
            }
          }
        }
      }
    }
  }

  /**
   * set the push relations (fields) by checking local fields against fields
   * from push config on host
   *
   * currently only taxonomy terms and file fields work
   *
   * if only terms that already exist in destination (by name) should be pushed,
   * you can set third party setting in dam_push form mode config like this
   * via config override in a settings.*.php on the host:
   * $config['core.entity_form_display.media.image.dam_push']['content']['field_tags']['third_party_settings']['drupal_dam']['only_existing'] = TRUE;
   * the variable part is the media type (image) and the taxonomy term field
   * name (field_tags), add a line for each media type and term field
   * that you want to override
   * Additionally you can use another setting to provide a default term (by name)
   * that is used if no term existing in destination is provided. That default
   * term have to exist on the DAM host of course
   * The setting is:
   * $config['core.entity_form_display.media.image.dam_push']['content']['field_tags']['third_party_settings']['drupal_dam']['default_tag'] = 'Tag string';
   * ToDo: Make this configurable via UI
   *
   * see @setSyncDisplayConfiguration()
   *
   * @param \Drupal\media\Entity\Media $media
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function setPushRelationships(Media $media) {
    $pushRelationships = [];
    $local_fields = $media->getFields();
    foreach ($this->pushFieldConfig as $field => $config) {
      if (isset($local_fields[$field])) {
        if (!empty($this->destinationSourceConfig['dam_authentication']['auth_enabled'])) {
          $this->damOAuthService->setAuthConfig($this->destinationSourceConfig['dam_authentication']['auth_enabled']);
          $this->headers = array_merge($this->headers, $this->getAuthHeader());
        }
        if ($local_fields[$field]->getFieldDefinition()
            ->getType() == 'entity_reference') {
          $settings = $local_fields[$field]->getFieldDefinition()->getSettings();
          if ($settings['target_type'] == 'taxonomy_term') {
            $target_bundle = reset($settings['handler_settings']['target_bundles']);
            /* @var \Drupal\drupal_dam\Service\DAMJSONTalkService $jsonTalk */
            $jsonTalk = \Drupal::service('drupal_dam.jsontalk');
            /* @var \Drupal\media\Entity\Media $destMedia */
            $destMedia = $this->entityTypeManager->getStorage('media')->create(['bundle' => $this->destinationMediaType->id()]);
            $jsonTalk->setMediaBundle($destMedia);
            $jsonTalk->setTaxonomyTermDestination($target_bundle, FALSE);
            // Look for default tag if configured
            if (isset($this->pushFieldConfig[$field]->third_party_settings->drupal_dam->default_tag)) {
              $default_pushValue = FALSE;
              $jsonTalk->unsetConditions();
              $jsonTalk->queryCondition('name', 'name', $this->pushFieldConfig[$field]->third_party_settings->drupal_dam->default_tag);
              $response = $jsonTalk->doJSONRequest();
              // Check if term exists
              if (isset($response->body->data[0]->id)) {
                $default_pushValue = [
                  'type' => $response->body->data[0]->type,
                  'id' => $response->body->data[0]->id
                ];
              }
            }
            foreach ($media->{$field} as $item) {
              $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($item->target_id);
              if ($term instanceof Term) {
                $pushValue = FALSE;
                $jsonTalk->unsetConditions();
                $jsonTalk->queryCondition('name', 'name', $term->name->value);
                $response = $jsonTalk->doJSONRequest();
                // Check if term exists
                if (isset($response->body->data[0]->id)) {
                  $pushValue = [
                    'type' => $response->body->data[0]->type,
                    'id' => $response->body->data[0]->id
                  ];
                }
                else {
                  if (!isset($this->pushFieldConfig[$field]->third_party_settings->drupal_dam->only_existing) || $this->pushFieldConfig[$field]->third_party_settings->drupal_dam->only_existing === FALSE) {
                    // create term if it does not exist
                    // ToDo: only term name is created no relations of term
                    $this->jsonRequest->setHTTPMethod('POST');
                    $this->jsonRequest->setRequestURI($this->getDestinationURL() . '/jsonapi/taxonomy_term/' . $target_bundle);
                    $headers = array_merge($this->getAuthHeader(), $this->headers);
                    $this->jsonRequest->addHeaders($headers);
                    $body = [
                      'data' => [
                        'type' => 'taxonony_term--' . $target_bundle,
                        'attributes' => [
                          'name' => $term->name->value
                        ]
                      ]
                    ];
                    $this->jsonRequest->setRequestBody(json_encode($body));
                    $this->jsonRequest->doRequest();
                    $response = $this->jsonRequest->getResponseBody();
                    $response_code = $this->jsonRequest->getResponseCode();
                    if (isset($response->data->id)) {
                      $pushValue = [
                        'type' => $response->data->type,
                        'id' => $response->data->id
                      ];
                    }
                    $this->jsonRequest->resetOptions();
                    $this->jsonRequest->setHTTPMethod('GET');
                  }
                }
                if (!empty($pushValue)) {
                  if ($local_fields[$field]->getFieldDefinition()
                    ->getFieldStorageDefinition()
                    ->isMultiple()) {
                    $pushRelationships[$field]['data'][] = $pushValue;
                  }
                  else {
                    $pushRelationships[$field]['data'] = $pushValue;
                  }
                }
              }
            }
            // Provide default tag if configured and no other tag was provided
            if (empty($pushRelationships[$field]['data']) && !empty($default_pushValue)) {
              if ($local_fields[$field]->getFieldDefinition()
                ->getFieldStorageDefinition()
                ->isMultiple()) {
                $pushRelationships[$field]['data'][] = $default_pushValue;
              }
              else {
                $pushRelationships[$field]['data'] = $default_pushValue;
              }
            }
          }
        }
        elseif ($local_fields[$field]->getFieldDefinition()
            ->getType() == 'file') {
          if ($field != $this->getDAMMediaSourceField()) {
            foreach ($media->{$field} as $item) {
              $pushfile = $this->entityTypeManager->getStorage('file')
                ->load($item->target_id);
              if ($pushfile instanceof File) {
                $this->jsonRequest->setHTTPMethod('POST');
                $this->jsonRequest->setRequestURI($this->getDestinationURL() . '/jsonapi/media/' . $this->getDestinationBundle() . '/' . $field);
                $this->jsonRequest->addHeaders($this->stepOneHeaders($pushfile->getFilename()));
                $this->jsonRequest->setRequestBody(file_get_contents($pushfile->getFileUri()));
                $this->jsonRequest->doRequest();
                $push_response = $this->jsonRequest->getResponseBody();
                $push_response_code = $this->jsonRequest->getResponseCode();
                if ($push_response_code == '201') {
                  $destination_UUID = $push_response->data->id;
                  $pushValue = [
                    'type' => 'file--file',
                    'id' => $destination_UUID,
                    'meta' => $this->getFileMetaData($item)
                  ];
                  if ($local_fields[$field]->getFieldDefinition()
                    ->getFieldStorageDefinition()
                    ->isMultiple()) {
                    $pushRelationships[$field]['data'][] = $pushValue;
                  }
                  else {
                    $pushRelationships[$field]['data'] = $pushValue;
                  }
                }
                else {
                  $this->logger->error(
                    'Error pushing asset to host (additional file -field %field). HTTP %code %message',
                    [
                      '%code' => $push_response_code,
                      '%message' => $this->jsonRequest->getErrorMessage(),
                      '%field' => $field
                    ]
                  );
                  $this->messenger->addError(
                    $this->t(
                      'Error creating asset (additional file - field %field) of type %type on host',
                      [
                        '%type' => $this->getDestinationBundle(),
                        '%field' => $field
                      ]
                    )
                  );
                }
              }
            }
            // Reset special request settings
            $this->jsonRequest->resetOptions();
            $this->jsonRequest->setHTTPMethod('GET');
          }
        }
      }
    }
    $this->pushRelationships = $pushRelationships;
  }

  /**
   * getter for destination settings - destination bundle
   *
   * @return mixed
   */
  protected function getDestinationBundle() {
    return $this->destinationSourceConfig['dam_settings']['dam_mediabundle'];
  }

  /**
   * getter for destination settings - destination address
   *
   * @return mixed
   */
  protected function getDestinationURL() {
    return $this->destinationSourceConfig['dam_settings']['dam_address'];
  }

  /**
   * setting the source field of destination bundle
   *
   * @return mixed
   *
   * ToDo: Attention this needs Administer media type permission
   * ToDo: Consolidate with DAMJSONTalkService
   */
  protected function setDAMMediaSourceField() {
    if (!empty($this->destinationSourceConfig['dam_authentication']['auth_enabled'])) {
      $this->damOAuthService->setAuthConfig($this->destinationSourceConfig['dam_authentication']['auth_enabled']);
    }
    $headers = array_merge($this->getAuthHeader(), $this->headersGetRequest());
    $this->jsonRequest->setHTTPMethod('GET');
    $this->jsonRequest->setRequestURI($this->getDestinationURL() . '/jsonapi/media_type/media_type?filter[drupal_internal__id][value]='.$this->getDestinationBundle());
    $this->jsonRequest->addHeaders($headers);
    $this->jsonRequest->doRequest();
    $mediaType = $this->jsonRequest->getResponseBody();
    $this->DAMMediaSourceField = $mediaType->data[0]->attributes->source_configuration->source_field;
  }

  /**
   * getting the source field of destination bundle
   * @return mixed
   */
  protected function getDAMMediaSourceField() {
    return $this->DAMMediaSourceField;
  }

  /**
   * get binary content of file to push
   *
   * @return false|string
   */
  protected function getPushFile() {
    return file_get_contents($this->pushFile->getFileUri());
  }

  /**
   * getting filename of file to push
   *
   * @return string
   */
  protected function getPushFileName() {
    return $this->pushFile->getFilename();
  }

  /**
   * preparing the body for post request to create media entity on dam host
   *
   * @param $destinationFileUUID
   *
   * @return array[]
   */
  protected function requestBodyStepTwo($destinationFileUUID) {
    return [
      'data' => [
        'type' => 'media--'.$this->getDestinationBundle(),
        'attributes' => $this->getPushAttributes(),
        'relationships' => $this->getPushRelationships() + [
          $this->getDAMMediaSourceField() => [
            'data' => [
              'type' => 'file--file',
              'id' => $destinationFileUUID,
              'meta' => $this->getSourceFieldMetaData()
            ]
          ]
        ]
      ]
    ];
  }

  /**
   * setting the config for push by reading entity form display from host
   *
   * the entity form display dam_push on the host is giving the information
   * about which fields can be pushed to the host
   */
  protected function setSyncDisplayConfiguration() {
    $headers = array_merge($this->getAuthHeader(), $this->headersGetRequest());
    $this->jsonRequest->setRequestURI($this->getDestinationURL() . '/jsonapi/entity_form_display/entity_form_display?filter[drupal_internal__id][value]=media.'.$this->getDestinationBundle().'.dam_push');
    $this->jsonRequest->addHeaders($headers);
    $this->jsonRequest->doRequest();
    $display = $this->jsonRequest->getResponseBody();
    $this->pushFieldConfig = get_object_vars($display->data[0]->attributes->content);
  }

  /**
   * getter function for push attributes
   *
   * @return array
   */
  protected function getPushAttributes() {
    return $this->pushAttributes;
  }

  /**
   * getter function for push relationships
   *
   * @return array
   */
  protected function getPushRelationships() {
    return $this->pushRelationships;
  }

  /**
   * getting meta data of source field
   *
   * see @requestBodyStepTwo()
   *
   * @return array
   */
  protected function getSourceFieldMetaData() {
    return $this->getFileMetaData($this->sourceFieldValue->first());
  }

  /**
   * getting meta data for a file field
   *
   * @param \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $item
   *
   * @return array
   */
  protected function getFileMetaData(EntityReferenceItem $item) {
    $meta = [];
    $meta_columns = $item->getFieldDefinition()->getFieldStorageDefinition()->getColumns();
    foreach ($meta_columns as $column => $definition) {
      if (!empty($item->{$column})) {
        $meta[$column] = $item->{$column};
      }
      else {
        $meta[$column] = $definition['default'] ?? '';
      }
    }
    return $meta;
  }

  /**
   * get special headers for getDAMMediaSourceField()
   *
   * @return string[]
   */
  protected function headersGetRequest() {
    return ['Accept' => 'application/json'];
  }

  /**
   * getting authentication header if applicable
   *
   * @return array|string[]
   */
  protected function getAuthHeader() {
    if ($this->damOAuthService->auth_enabled()) {
      return [
        'Authorization' => \Drupal::state()
            ->get($this->damOAuthService->getAuthConfig()->getName().'.token.type') . ' ' . \Drupal::state()
            ->get($this->damOAuthService->getAuthConfig()->getName().'.token.token')
      ];
    }
    else {
      return [];
    }
  }

  /**
   * headers for push step one (pushing a file to DAM Host)
   *
   * @return array|string[]
   */
  protected function stepOneHeaders($filename = FALSE) {
    if (empty($filename)) {
      $filename = $this->getPushFileName();
    }
    $headers = $this->headers;
    $headers['Content-Disposition'] = 'file; filename="' . $filename .'"';
    $headers['Content-Type'] = 'application/octet-stream';
    return $headers;
  }

}
