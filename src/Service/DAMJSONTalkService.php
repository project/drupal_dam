<?php

namespace Drupal\drupal_dam\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media\Entity\Media;
use Drupal\media\MediaTypeInterface;
use Psr\Log\LoggerInterface;


class DAMJSONTalkService {

  /**
   * @var string[]
   * http headers
   */
  protected $headers = ['Accept' => 'application/json'];

  /**
   * @var Media
   */
  protected $media;

  /**
   * @var \Drupal\media\MediaSourceInterface
   */
  protected $mediaSource;

  /**
   * @var array
   * media source configuration
   */
  protected $mediaSourceConfiguration;

  /**
   * @var \Drupal\media\Entity\MediaType
   */
  protected $mediaType;

  /**
   * @var array
   * holding the query parameters
   */
  protected $queryArray = [];

  /**
   * @var string
   * $queryArray converted to string for URL
   */
  protected $queryString;

  /**
   * @var string
   * the destination for the JSON-API call.
   * entity, bundle, uuid, field
   */
  protected $jsonDestination;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\drupal_dam\Service\DAMOAuthService
   */
  protected $damOAuthService;

  /**
   * @var \Drupal\drupal_dam\Service\JSONRequestService
   */
  protected $jsonRequest;

  /**
   * @var LoggerInterface
   */
  protected $logger;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  use StringTranslationTrait;

  /**
   * DAMJSONTalkService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\drupal_dam\Service\DAMOAuthService $dam_oauth_service
   * @param \Drupal\drupal_dam\Service\JSONRequestService $json_request
   * @param \Psr\Log\LoggerInterface $logger
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DAMOAuthService $dam_oauth_service, JSONRequestService $json_request, LoggerInterface $logger, MessengerInterface $messenger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->damOAuthService = $dam_oauth_service;
    if ($this->damOAuthService->auth_enabled()) {
      $this->setAuthHeader();
    }
    $this->jsonRequest = $json_request;
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * setting parameters for media based query
   *
   * ToDo: Needs to be run for authenticating in most places, maybe take this out.
   *
   * @param \Drupal\media\Entity\Media $media
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function setMediaBundle(Media $media) {
    $this->media = $media;
    $this->setMediaSource();
    $this->mediaType = $this->entityTypeManager->getStorage('media_type')->load($this->media->bundle());
    $this->setMediaTypeAuthHeader();
  }

  /**
   * setting destination for Search API query
   */
  public function setSearchAPIIndex() {
    $this->setJSONDestination('index', $this->mediaSourceConfiguration['dam_search']['search_index']);
  }

  /**
   * setting destination for a media bundle.
   *
   * not limited to a specific media entity
   *
   * @param $bundle
   */
  public function setMediaBundleDestination($bundle) {
    $this->setJSONDestination('media', $bundle);
  }

  /**
   * setting destination for taxonomy term query
   *
   * @param $vid
   *   taxonomy vocabular machine name
   * @param $uuid
   *   taxonomy term UUID
   */
  public function setTaxonomyTermDestination($vid, $uuid) {
    $this->setJSONDestination('taxonomy_term', $vid, $uuid);
  }

  /**
   * setting destination for file entity query
   *
   * @param $uuid
   *   file entity UUID
   */
  public function setFileDestination($uuid) {
    $this->setJSONDestination('file', 'file', $uuid);
  }

  /**
   * adding a query condition (filter) for JSON API request
   *
   * @param $name
   * @param $path
   * @param $value
   * @param string $operator
   * @param string $type
   */
  public function queryCondition($name, $path, $value, $operator = '=', $type = 'filter') {
    if ($path === FALSE) {
      $this->queryArray[$type.'['.$name.']'] = $value;
    }
    else {
      $this->queryArray[$type.'[' . $name . '][condition][path]'] = $path;
      $this->queryArray[$type.'[' . $name . '][condition][operator]'] = $operator;
      if (is_array($value)) {
        foreach ($value as $val) {
          $query[$type.'[' . $name . '][condition][value]'] = [];
          $this->queryArray[$type.'[' . $name . '][condition][value]'][] = $val;
        }
      }
      else {
        $this->queryArray[$type.'[' . $name . '][condition][value]'] = $value;
      }
    }
  }

  /**
   * adding a query condition (sort) to JSON API request
   *
   * @param $name
   * @param $path
   * @param $direction
   *
   * @return void
   */
  public function querySort($name = 'sort-cond', $path = 'created', $direction = 'DESC') {
    $this->queryArray['sort'.'['.$name.'][path]'] = $path;
    $this->queryArray['sort'.'['.$name.'][direction]'] = $direction;
  }

  /**
   * unset the JSON API query conditions
   */
  public function unsetConditions() {
    $this->queryArray = [];
  }

  /**
   * setting JSON page limit
   *
   * @param int $limit
   */
  public function queryPageLimit($limit = 10) {
    $this->queryArray['page[limit]'] = $limit;
  }

  /**
   * adding field to JSON destination
   *
   * needs to be done after setJSONDestination
   * adds a field to the end of the destination, delivering only this
   * fields information in response. this is for relationship fields like
   * files/images or taxonomy term
   *
   * ToDo: Should this be done via ?include=field instead?
   *
   * @param $field
   */
  public function addJSONDestination($field) {
    $this->jsonDestination .= '/' . $field;
  }

  /**
   * sending the JSON API request
   *
   * @return object
   *   response object
   */
  public function doJSONRequest() {
    // return an empty object if no DAM address is set
    if ($this->getDAMAddress() === FALSE) {
      return new \stdClass();
    }
    // default sorting - only for JSON API requests to media entities
    if (substr($this->getJSONDestination(), 0, 5) === 'media') {
      $this->querySort('default-sort', $this->mediaSourceConfiguration['dam_sort']['path'], $this->mediaSourceConfiguration['dam_sort']['direction']);
      // need this to avoid missing/duplicate media if default-sort is the same
      // for multiple media (paging effect)
      $this->querySort('fallback-sort', 'drupal_internal__mid');
    }
    \Drupal::moduleHandler()->alter('drupal_dam_json_request', $this);
    $this->jsonRequest->setRequestURI($this->getDAMAddress() . '/jsonapi/' . $this->getJSONDestination() . $this->getQueryString());
    $this->jsonRequest->addHeaders($this->getHeaders());
    $this->jsonRequest->doRequest();
    if ($this->jsonRequest->getResponseCode() == 200) {
      $response = new \stdClass();
      $response->body = $this->jsonRequest->getResponseBody();
      return $response;
    }
    else {
      // something went wrong, try to reauthenticate
      if ($this->damOAuthService->auth_enabled()) {
        $this->damOAuthService->client_credentials_grant();
      }
      $this->logger->error(
        'Error on JSON Request. HTTP %message',
        [
          '%message' => $this->jsonRequest->getErrorMessage()
        ]
      );
      $this->messenger->addError($this->t('Error on JSON Request %error', ['%error' => $this->jsonRequest->getErrorMessage()]));
      return $this->jsonRequest->getResponseBody();
    }
  }

  /**
   * getter for media type
   *
   * @return \Drupal\media\Entity\MediaType
   */
  public function getMediaType() {
    return $this->mediaType;
  }
  /**
   * getting the source field of destination bundle
   *
   * @return mixed
   *
   * ToDo: Attention this needs Administer media type permission
   */
  public function getDAMMediaSourceField() {
    $this->jsonRequest->setRequestURI($this->getDAMAddress() . '/jsonapi/media_type/media_type?filter[drupal_internal__id][value]='.$this->getDAMMediaBundle());
    $this->jsonRequest->addHeaders($this->getHeaders());
    $this->jsonRequest->doRequest();
    $mediaType = $this->jsonRequest->getResponseBody();
    return $mediaType->data[0]->attributes->source_configuration->source_field;
  }

  /**
   * checking for valid configuration of JSON Request
   *
   * @param $address
   * @param $bundle
   * @param $auth_config
   *
   * @return array
   */
  public function checkJSON($address, $bundle, $auth_config) {
    $status = [];
    if (!empty($auth_config['auth_enabled'])) {
      $this->damOAuthService->setAuthConfig($auth_config['auth_enabled']);
      $this->setAuthHeader();
    }
    $this->jsonRequest->addHeaders($this->getHeaders());
    $this->jsonRequest->setRequestURI($address . '/jsonapi/media/' . $bundle);
    $this->jsonRequest->doRequest();
    if ($this->jsonRequest->getResponseCode() == 200) {
      $status['error'] = FALSE;
    }
    else {
      $status['error'] = TRUE;
      $status['message'] = $this->t('Please check your configuration: @errormessage', ['@errormessage' => $this->jsonRequest->getErrorMessage()]);
    }
    return $status;
  }

  /**
   * getter for headers
   *
   * @return string[]
   */
  protected function getHeaders() {
    return $this->headers;
  }

  /**
   * set Media type based authentication.
   *
   * this is the new type of authentication.
   */
  protected function setMediaTypeAuthHeader() {
    if ($this->mediaType instanceof MediaTypeInterface) {
      if (!(empty($this->mediaType->getSource()
        ->getConfiguration()['dam_authentication']['auth_enabled']))) {
        $this->damOAuthService->setAuthConfig($this->mediaType->getSource()
          ->getConfiguration()['dam_authentication']['auth_enabled']);
        $this->setAuthHeader();
      }
    }
  }

  /**
   * setting authentication header to header variable
   */
  protected function setAuthHeader() {
    $this->headers['Authorization'] = \Drupal::state()->get($this->damOAuthService->getAuthConfig()->getName().'.token.type') . ' ' . \Drupal::state()->get($this->damOAuthService->getAuthConfig()->getName().'.token.token');
  }

  /**
   * setting media parameters
   */
  protected function setMediaSource() {
    $this->mediaSource = $this->media->getSource();
    $this->mediaSourceConfiguration = $this->mediaSource->getConfiguration();
    $this->setJSONDestination($this->media->getEntityTypeId(), $this->getDAMMediaBundle(), $this->media->{$this->mediaSourceConfiguration['source_field']}->value);
  }

  /**
   * getter for media source settings - dam address
   *
   * @return mixed
   */
  protected function getDAMAddress() {
    return $this->mediaSourceConfiguration['dam_settings']['dam_address'];
  }

  /**
   * getter from media source settings - dam bundle
   *
   * @return mixed
   */
  protected function getDAMMediaBundle() {
    return $this->mediaSourceConfiguration['dam_settings']['dam_mediabundle'];
  }

  /**
   * setting JSON destination
   *
   * @param string $entity_type
   * @param bool $bundle
   * @param bool $uuid
   */
  protected function setJSONDestination($entity_type = 'media', $bundle = FALSE, $uuid = FALSE) {
    if (!$bundle) {
      $bundle = $this->getDAMMediaBundle();
    }
    $this->jsonDestination = $entity_type. '/' . $bundle;
    if ($uuid) {
      $this->jsonDestination .= '/'. $uuid;
    }
  }

  /**
   * getter for JSON destination
   *
   * @return string
   */
  protected function getJSONDestination() {
    return $this->jsonDestination;
  }

  /**
   * build the JSON API query string from query array
   *
   * @return string
   */
  protected function getQueryString() {
    $this->queryString = '';
    foreach ($this->queryArray as $parameter => $value) {
      ($this->queryString == '') ? $this->queryString .= '?' : $this->queryString .= '&';
      if (is_array($value)) {
        foreach ($value as $val) {
          if (substr($this->queryString, -1) != '&') {
            $this->queryString .= '&';
          }
          $this->queryString .= $parameter . '[]=' . $val;
        }
      }
      else {
        $this->queryString .= $parameter . '=' . $value;
      }
    }
    return $this->queryString;
  }

}
