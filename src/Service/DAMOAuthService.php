<?php

namespace Drupal\drupal_dam\Service;

use Drupal\Core\Config\ConfigFactoryInterface;

class DAMOAuthService {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $authConfig;

  /**
   * @var string | object
   *
   * decoded json
   */
  protected $tokenResponse;

  /**
   * DAMOAuthService constructor.
   */
  public function __construct() {
    // This is the old deprecated authentication (one for all)
    $this->setAuthConfig('drupal_dam.auth');
  }

  /**
   * set Authentication config and request token if expired
   *
   * @param $config_name
   */
  public function setAuthConfig($config_name) {
    $this->authConfig = \Drupal::config($config_name);
    if ($this->auth_enabled()) {
      $expiry = \Drupal::state()->get($this->getAuthConfig()->getName().'.token.expire') ? \Drupal::state()->get($this->getAuthConfig()->getName().'.token.expire') : time();
      if(time() >= $expiry) {
        $this->client_credentials_grant();
      }
    }
  }

  public function getAuthConfig() {
    return $this->authConfig;
  }

  /**
   * @return bool
   */
  public function auth_enabled() {
    return $this->authConfig->get('auth_enabled') || $this->authConfig->get('id');
  }

  /**
   * get credentials from OAuth
   */
  public function client_credentials_grant() {
    $grant_type = 'client_credentials';
    $client_id = $this->authConfig->get('client_id');
    $client_secret = $this->authConfig->get('client_secret');
    $base_url = $this->authConfig->get('auth_url');
    try {
      $response = \Drupal::httpClient()->post(
        $base_url . '/oauth/token', [
          'verify' => FALSE,
          'form_params' => [
            'grant_type' => $grant_type,
            'client_id' => $client_id,
            'client_secret' => $client_secret
          ],
          'headers' => [
            'Content-type' => 'application/x-www-form-urlencoded'
          ]
        ]
      )->getBody()->getContents();
    }
    catch (\RuntimeException $exception) {
      $response = FALSE;
    }
    if ($response) {
      $this->tokenResponse = json_decode($response);
      \Drupal::state()->set($this->getAuthConfig()->getName().'.token.type', $this->getTokenType());
      \Drupal::state()->set($this->getAuthConfig()->getName().'.token.token', $this->getAccessToken());
      \Drupal::state()->set($this->getAuthConfig()->getName().'.token.expire', time() + $this->getExpiresIn());
    }
    else {
      $this->tokenResponse = FALSE;
    }

  }

  /**
   * token type getter
   *
   * @return string | false
   */
  protected function getTokenType() {
    return $this->tokenResponse ? $this->tokenResponse->token_type : FALSE;
  }

  /**
   * token getter
   *
   * @return string | false
   */
  protected function getAccessToken() {
    return $this->tokenResponse ? $this->tokenResponse->access_token : FALSE;
  }

  /**
   * token expiration time getter
   *
   * @return int
   */
  protected function getExpiresIn() {
    return $this->tokenResponse ? $this->tokenResponse->expires_in : 0;
  }
}
