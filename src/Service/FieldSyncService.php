<?php

namespace Drupal\drupal_dam\Service;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\media\Entity\Media;
use Drupal\taxonomy\Entity\Term;

class FieldSyncService {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\drupal_dam\Service\DAMJSONTalkService
   */
  protected $jsonTalk;

  /**
   * @var \Drupal\drupal_dam\Service\DAMFileRetrieveService
   */
  protected $damFileRetrieveService;

  /**
   * @var \Drupal\Core\Entity\Entity\EntityFormDisplay
   */
  protected $damSyncFormDisplay;

  /**
   * @var string
   */
  protected $damSyncFormDisplayMachineName = 'dam_sync';

  /**
   * @var array
   *
   * locally configured components from $damSyncFormDisplay
   *
   * see EntityDipslayInterface::getComponents()
   */
  protected $components;

  /**
   * local field configurations
   *
   * @var array of \Drupal\field\Entity\FieldStorageConfig
   *
   */
  protected $fieldStorageConfigs;

  /**
   * attributes from JSON API
   *
   * @var array
   */
  protected $attributes;

  /**
   * @var array
   *
   * relationships from JSON API
   */
  protected $relations;

  /**
   * FieldSyncService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   * @param \Drupal\drupal_dam\Service\DAMJSONTalkService $json_talk
   * @param \Drupal\drupal_dam\Service\DAMFileRetrieveService $dam_file_retrieve_service
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, DAMJSONTalkService $json_talk, DAMFileRetrieveService $dam_file_retrieve_service) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->damFileRetrieveService = $dam_file_retrieve_service;
    $this->jsonTalk = $json_talk;
  }

  /**
   * initiate JSON field sync
   *
   * @param \Drupal\media\Entity\Media $entity
   *
   * @param array $filter
   *   allows to filter elements that are configured in sync form display
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function initFieldSync(Media $entity, $filter = []) {
    $this->damSyncFormDisplay = $this->entityTypeManager->getStorage(
      'entity_form_display')
      ->load($entity->getEntityTypeId().'.'.$entity->bundle().'.'.$this->damSyncFormDisplayMachineName
      );
    $this->jsonTalk->setMediaBundle($entity);
    $response = $this->jsonTalk->doJSONRequest();
    $this->attributes = (array) $response->body->data->attributes;
    $this->relations = (array) $response->body->data->relationships;
    $this->fieldStorageConfigs = $this->entityFieldManager
      ->getFieldStorageDefinitions($entity->getEntityTypeId());
    $this->setComponents($filter);
    $this->syncRelationships($entity);
    $this->syncRegularFields($entity);
    $media_Type = $this->jsonTalk->getMediaType();
    $attributes_and_relations = [
      'attributes' => $this->attributes,
      'relations' => $this->relations
    ];
    \Drupal::moduleHandler()->alter('drupal_dam_field_sync', $entity,  $media_Type, $attributes_and_relations);
  }

  /**
   * setting components (local part) that are to be synced
   *
   * @param array $filter
   *   allows to filter elements that are configured in sync form display
   */
  protected function setComponents($filter = []) {
    // Getting components (fields) that are configured in the $damSyncFormDisplayMachineName view mode
    $this->components = $this->damSyncFormDisplay->getComponents();
    // Remove default keys (fields) that are not configured through form display
    if (isset($this->components['langcode'])) {
      unset($this->components['langcode']);
    }
    if (isset($this->components['revision_log_message'])) {
      unset($this->components['revision_log_message']);
    }
    if (!empty($filter)) {
      foreach ($this->components as $compkey => $component) {
        if (!in_array($compkey, $filter)) {
          unset($this->components[$compkey]);
        }
      }
    }
  }

  /**
   * syncing relationships between local and remote
   *
   * Parsing Display components and check for them in DAM relationships
   * assign DAM relationships to local field if local match is found, create
   * local relations if necessary
   * this is currently only working for taxonomy term relations and file
   * relations
   *
   * @param \Drupal\media\Entity\Media $entity
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function syncRelationships(Media $entity) {
    // first get and sync meta data for local copy field
    $damSourceField = $this->jsonTalk->getDAMMediaSourceField();
    $localCopyField = $entity->getSource()->getConfiguration()['localcopy_field'];
    $entity->{$localCopyField} = (array)$this->relations[$damSourceField]->data->meta;

    foreach ($this->components as $compkey => $component) {
      foreach ($this->relations as $relkey => $relation) {
        if ($compkey == $relkey) {
          // Single or multivalue
          if (is_object($relation->data)) {
            $relationdata = [$relation->data];
          }
          else {
            $relationdata = $relation->data;
          }
          if (!empty($relationdata)) {
            foreach ($relationdata as $reldata) {
              $type = explode('--', $reldata->type);
              if ($type[0] == 'taxonomy_term') {
                $id = $reldata->id;
                $this->jsonTalk->unsetConditions();
                $this->jsonTalk->setTaxonomyTermDestination($type[1], $id);
                $taxResponse = $this->jsonTalk->doJSONRequest();
                if (isset($taxResponse->body->data->attributes->name)) {
                  $loadterm = $this->entityTypeManager
                    ->getStorage('taxonomy_term')
                    ->loadByProperties(
                      [
                        'name' => $taxResponse->body->data->attributes->name,
                        'vid' => $type[1]
                      ]
                    );
                  if (empty($loadterm)) {
                    $term = Term::create([
                      'name' => $taxResponse->body->data->attributes->name,
                      'vid' => $type[1]
                    ]);
                    $term->save();
                  }
                  else {
                    $term = array_shift($loadterm);
                  }
                  $target_id = [
                    'target_id' => $term->id()
                  ];
                  $entity->{$compkey}->appendItem($target_id);
                }
              }
              elseif ($type[0] == 'file') {
                $id = $reldata->id;
                $this->jsonTalk->unsetConditions();
                $this->jsonTalk->setFileDestination($id);
                $fileResponse = $this->jsonTalk->doJSONRequest();
                $retrieved_file = $this->damFileRetrieveService->retrieveFileURL($fileResponse, $compkey, $entity);
                $meta = [];
                foreach ($relation->data->meta as $metakey => $metainfo) {
                  $meta[$metakey] = $metainfo;
                }
                $meta['target_id'] = $retrieved_file->fid->value;
                if ($retrieved_file) {
                  $entity->{$compkey}->appendItem($meta);
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * syncing attributes/regular fields between local and remote
   *
   * Parsing Display components and check for them in DAM attributes, assign
   * DAM attributes to local field if local match is found
   *
   * @param \Drupal\media\Entity\Media $entity
   */
  protected function syncRegularFields(Media $entity) {
    foreach ($this->components as $compkey => $component) {
      foreach ($this->attributes as $attrkey => $attribute) {
        if ($compkey == $attrkey) {
          if (!empty($attribute)) {
            /* @var \Drupal\field\Entity\FieldStorageConfig $fieldStorageConfig */
            $fieldStorageConfig = $this->fieldStorageConfigs[$compkey];
            $fieldColumns = $fieldStorageConfig->getColumns();
            $fieldMultiple = $fieldStorageConfig->isMultiple();
            if ($fieldMultiple) {
              foreach ($attribute as $attribValues) {
                if (is_object($attribValues)) {
                  $values = [];
                  foreach ($fieldColumns as $column => $definition) {
                    $values[$column] = $attribValues->{$column};
                  }
                  $entity->{$compkey}->appendItem($values);
                }
                else {
                  $entity->{$compkey}[] = $attribValues;
                }
              }
            }
            else {
              if (is_object($attribute)) {
                $values = [];
                foreach ($fieldColumns as $column => $definition) {
                  if (property_exists($attribute, $column)) {
                    $values[$column] = $attribute->{$column};
                  }
                }
                $entity->{$compkey} = $values;
              }
              else {
                $entity->{$compkey} = $attribute;
              }
            }
          }
        }
      }
    }
  }

}
