<?php

namespace Drupal\drupal_dam\Service;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;

/**
 * Class JSONRequestService
 *
 * basic class for doing JSON requests with Drupal http_client (Guzzle)
 *
 * @package Drupal\drupal_dam\Service
 */
class JSONRequestService {

  use StringTranslationTrait;

  /**
   * Drupal HTTP client service
   *
   * @var \GuzzleHttp\Client
   */
  protected $guzzleClient;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;


  /**
   * @var \GuzzleHttp\Psr7\Response
   */
  protected $httpResponse;

  /**
   * @var string
   *   HTTP response code
   */
  protected $httpResponseCode;

  /**
   * @var string
   *   exception message if it's an error beyond HTTP code
   */
  protected $exceptionMessage;

  /**
   * @var string
   */
  protected $httpMethod = 'GET';

  /**
   * @var string
   */
  protected $requestURI;

  /**
   * @var array
   */
  protected $requestOptions = [];

  /**
   * JSONRequestService constructor.
   *
   * @param \GuzzleHttp\Client $http_client
   * @param \Psr\Log\LoggerInterface $logger
   */
  public function __construct(Client $http_client,  LoggerInterface $logger) {
    $this->guzzleClient = $http_client;
    $this->logger = $logger;
  }

  /**
   * set HTTP method, if not using GET
   *
   * @param $method string
   *   HTTP method
   */
  public function setHTTPMethod($method) {
    $this->httpMethod = $method;
  }

  /**
   * reset all request options
   */
  public function resetOptions() {
    $this->requestOptions = [];
  }

  /**
   * set the URL for the request
   *
   * @param $url
   */
  public function setRequestURI($url) {
    $this->requestURI = $url;
  }

  /**
   * @param array $headers
   *   array of HTTP headers
   */
  public function addHeaders(array $headers) {
    if (!isset($this->requestOptions['headers'])) {
      $this->requestOptions['headers'] = [];
    }
    $this->requestOptions['headers'] = array_merge($this->requestOptions['headers'], $headers);
  }

  public function setRequestBody($body) {
    $this->requestOptions['body'] = $body;
  }

  /**
   * doing a JSON Request, service needs to be initiated with other methods first
   */
  public function doRequest() {
    try {
        $this->httpResponse = $this->guzzleClient->request(
          $this->httpMethod,
          $this->requestURI,
          $this->requestOptions
        );
      $this->httpResponseCode = $this->httpResponse->getStatusCode();
    } catch (GuzzleException $e) {
      $this->httpResponseCode = $e->getCode();
      if ($e->getCode() == 0) {
        $this->exceptionMessage = $e->getMessage();
      }
      $this->logger->error(
        'Error during JSON request: %message - Request: %request',
        [
          '%message' => $this->getErrorMessage(),
          '%request' => $this->requestURI
        ]
      );
      $this->httpResponse = FALSE;
    }
  }

  /**
   * get the HTTP response or FALSE on error
   *
   * @return \GuzzleHttp\Psr7\Response | bool
   */
  public function getResponse() {
    return $this->httpResponse;
  }

  /**
   * return JSON decoded HTTP Response body or FALSE on error
   *
   * @return bool|mixed
   */
  public function getResponseBody() {
    return $this->httpResponse ? json_decode((string) $this->httpResponse->getBody()) : FALSE;
  }

  /**
   * get the HTTP response code
   *
   * @return string
   */
  public function getResponseCode() {
    return $this->httpResponseCode;
  }

  /**
   * get Error message on HTTP response
   *
   * @return string
   */
  public function getErrorMessage() {
    if ($this->httpResponseCode == 0) {
      return $this->exceptionMessage;
    }
    else {
      return $this->t('HTTP code ') . $this->httpResponseCode;
    }
  }








}
