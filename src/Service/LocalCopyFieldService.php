<?php

namespace Drupal\drupal_dam\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\drupal_dam\Plugin\media\Source\DAMFile;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;

/**
 * Class LocalCopyFieldService
 *
 * getting the LocalCopyField for DAM media sources
 *
 * @package Drupal\drupal_dam\Service
 */
class LocalCopyFieldService {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * LocalCopyFieldService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * getting the LocalCopyField for a DAM media source
   *
   * this is the field that holds the local file copy of the DAM asset,
   * for other media source types the source field is returned
   *
   * @param \Drupal\media\MediaInterface $media
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getLocalCopyField(MediaInterface $media) {
    $media_type = $this->entityTypeManager->getStorage('media_type')->load($media->bundle());
    if ($media_type instanceof MediaTypeInterface) {
      $media_source = $media->getSource();
      if ($media_source instanceof DAMFile) {
        return $media_source->getLocalCopyFieldDefinition($media_type);
      }
      else {
        return $media_source->getSourceFieldDefinition($media_type);
      }
    }
    return null;
  }

}
