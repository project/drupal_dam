<?php

namespace Drupal\drupal_dam\Service;

use Drupal\Core\Link;
use Drupal\Core\StringTranslation\ByteSizeMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\media\Entity\MediaType;

class ResponseFormatterService {

  use StringTranslationTrait;
  /**
   * @var object
   *   HTTP response body
   */
  protected $response;

  /**
   * @var \Drupal\media\Entity\MediaType
   */
  protected $mediaType;

  /**
   * if true a reset link will be delivered
   *
   *   reset link goes back to initial list of assets without any filters
   *   (searches)
   *
   * @var bool
   */
  protected $resetOption = FALSE;

  /**
   * pagination count to use for the lists
   *
   * @var int
   */
  protected $paginationCount = 10;

  /**
   * @var int
   *   item count for search results
   */
  protected $itemCount = 0;

  /**
   * defines whether to allow to create new entities of already existing ones
   *
   *   this is determined by UUID from DAM and local media type
   *
   * @var bool
   */
  protected $allowRecreation = TRUE;

  /**
   * @param $response
   * @param \Drupal\media\Entity\MediaType $media_type
   */
  public function setResponse($response, MediaType $media_type) {
    $this->response = $response;
    $this->mediaType = $media_type;
  }

  /**
   * indicate whether rendered content should include reset link
   *
   * @param $reset
   */
  public function setReset($reset) {
    $this->resetOption = $reset;
  }

  /**
   * see @$allowRecreation
   *
   * @param bool $recreate
   */
  public function setAllowRecreation($recreate) {
    $this->allowRecreation = $recreate;
  }

  /**
   * specify the pagination count
   *
   * @param int $pagination_count
   */
  public function setPaginationCount($pagination_count) {
    $this->paginationCount = $pagination_count;
  }

  /**
   * set item count from response data
   *
   * @param $data
   *   json api result data, count info in meta data
   */
  protected function setItemCount($data) {
    $this->itemCount = $data->meta->count ?? 0;
  }

  /**
   * wrapper for getting the JSON response data
   *
   * @return object
   *   JSON response data
   */
  protected function getResponseData() {
    return isset($this->response->body) ? $this->response->body : $this->response;
  }

  /**
   * @param bool $use_existing
   *   indicates whether to allow to recreate already locally existing entities
   *   only valid for entity browser setup with DAMBrowser widget
   *
   * @return array|string[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getItemList($use_existing = FALSE) {
    $data = $this->getResponseData();
    $this->setItemCount($data);
    if (empty($data)) {
      return [
        '#markup' => $this->t('Data delivery error')
      ];
    }
    // Special case - no DAM address set, assuming to be disabled
    if (is_object($data) && empty((array)$data)) {
      return [
        '#markup' => $this->t('Disabled')
      ];
    }

    $items = [];
    $existing = \Drupal::entityTypeManager()->getStorage('media')
      ->loadByProperties(['bundle' => $this->mediaType->id()]);
    $sourceConfig = $this->mediaType->getSource()->getConfiguration();
    $existingUUIDs = [];
    foreach($existing as $exist) {
      $existingUUIDs[] = $exist->{$sourceConfig['source_field']}->value;
    }
    foreach ($data->data as $key => $media) {
      $thumb_src = $media->attributes->dam_thumbnail_url->url;
      // Replace internal DAM url with external DAM url for thumbnails
      if ($sourceConfig['dam_settings']['dam_address_public']) {
        $damurl = parse_url($thumb_src);
        $publicurl = parse_url($sourceConfig['dam_settings']['dam_address_public']);
        $damurl = $this->replace_internal_dam_url($damurl, $publicurl);
        $thumb_src = $this->reparse_url($damurl);
      }
      $exists = FALSE;
      $exists_class = FALSE;
      if (in_array($media->id, $existingUUIDs)) {
        $exists = TRUE;
        $status = $this->allowRecreation ? 'active' : 'inactive';
        $exists_class = 'exists';
      }
      else {
        $status = 'active';
      }
      $thumb = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['dam-item', $exists_class]
        ]
      ];
      $thumb['damitem'] = [
        '#theme' => 'image',
        '#uri' => $thumb_src,
        '#attributes' => [
          'data-uuid' => $media->id,
          'class' => [$status]
        ],
        '#suffix' => '<div class="dam-item-check"></div>'
      ];
      if ($exists && $use_existing) {
        $thumb['recreate'] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['recreate', 'hidden']
          ]
        ];
        $thumb['recreate']['label'] = [
          '#markup' => $this->t('Create new entity')
        ];
        $thumb['recreate']['checkbox'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Create new entity'),
          '#attributes' => [
            'disabled' => 'disabled'
          ],
          '#theme_wrappers' => []
        ];
      }
      $items[] = [
        '#theme' => 'dam_filewidget_item',
        '#thumb' => $thumb,
        '#label' => $sourceConfig['dam_display']['item_label'] ? $media->attributes->name : FALSE,
        '#filesize' => !empty($sourceConfig['dam_display']['item_file_size']) ? $media->attributes->dam_file_size ? ByteSizeMarkup::create($media->attributes->dam_file_size) : FALSE : FALSE,
        '#filemime' => !empty($sourceConfig['dam_display']['item_file_mime']) ? $media->attributes->dam_file_mimetype ?? FALSE : FALSE,
      ];
    }
    $element = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['dam-wrapper', 'auswahlliste']
      ]
    ];

    if (!empty($this->itemCount) && !empty($sourceConfig['dam_display']['page_counter'])) {
      $pages_item_label = $this->formatPlural($this->itemCount, '1 item', '@count items', [], ['context' => 'Drupal DAM']);
      $pages_count = ceil($this->itemCount/$this->paginationCount);
      $pages_label = $pages_count > 1 ? $this->t('@items on @pages pages', ['@items' => $pages_item_label, '@pages' => $pages_count]) : $pages_item_label;
    }

    $element['liste'] = [
      '#theme' => 'dam_filewidget',
      '#items' => $items,
      '#sorting' => !empty($sourceConfig['dam_sort']['sort_options']) ? [
        '#theme' => 'dam_filewidget_sorting',
        '#sorting' => $this->getSorting($use_existing),
        '#label' => $this->t('Sorting (@current)', ['@current' => $this->getCurrentSortLabel() ])
      ] : NULL,
      '#pager' => $this->getPager($use_existing, $sourceConfig['dam_display']['pager_size']),
      '#pages' => $pages_label ?? NULL,
      '#current_page' => !empty($this->getCurrentPage()) ? $this->t('Page @current_page', ['@current_page' => $this->getCurrentPage()]) : NULL,
    ];
    return $element;
  }

  /**
   * get label for current sorting
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   */
  protected function getCurrentSortLabel() {
    $data = $this->getResponseData();
    $url_parts = parse_url($data->links->self->href);
    parse_str($url_parts['query'], $query);
    $sourceConfig = $this->mediaType->getSource()->getConfiguration();
    $sort_options = $sourceConfig['dam_sort']['sort_options'] ?? [];
    if (!empty($sort_options)) {
      $option_label = $sort_options[$query['sort']['default-sort']['path']];
      if ($query['sort']['default-sort']['direction'] === 'ASC') {
        return $this->t('@option_label ascending', ['@option_label' => $option_label]);
      }
      else {
        return $this->t('@option_label descending', ['@option_label' => $option_label]);
      }
    }
    return '';
  }

  /**
   * get sorting options
   *
   * @param $use_existing
   *
   * @return array
   *   array of pagination link render arrays
   */
  protected function getSorting($use_existing) {
    $sourceConfig = $this->mediaType->getSource()->getConfiguration();
    $sort_options = $sourceConfig['dam_sort']['sort_options'] ?? [];
    $data = $this->getResponseData();
    $url_parts = parse_url($data->links->self->href);
    parse_str($url_parts['query'], $query);
    $links = [];
    foreach ($sort_options as $sort_option => $option_label) {
      $page_query = $query;
      $page_query['sort']['default-sort'] = [
        'path' => $sort_option,
        'direction' => 'ASC'
      ];
      $url_parts['query'] = http_build_query($page_query);
      $links[] = Link::createFromRoute($this->t('@option_label ascending', ['@option_label' => $option_label]), 'drupal_dam.paginate', ['mediatype' => $this->mediaType->id()], $this->paginationLinkOptions($use_existing, $this->reparse_url($url_parts)));
      $page_query['sort']['default-sort']['direction'] = 'DESC';
      $url_parts['query'] = http_build_query($page_query);
      $links[] = Link::createFromRoute($this->t('@option_label descending', ['@option_label' => $option_label]), 'drupal_dam.paginate', ['mediatype' => $this->mediaType->id()], $this->paginationLinkOptions($use_existing, $this->reparse_url($url_parts)));
    }
    return $links;
  }

  /**
   * get the current page from response data
   *
   * @return false|int
   */
  protected function getCurrentPage() {
    $data = $this->getResponseData();
    $url_parts = parse_url($data->links->self->href);
    parse_str($url_parts['query'], $query);
    if (!empty($this->itemCount)) {
      $pages_count = ceil($this->itemCount/$this->paginationCount);
      if ($pages_count > 1) {
        $offset = (int) ($query['page']['offset'] ?? 0);
        $current_page = (int) ($offset / $this->paginationCount) + 1;
      }
    }

    return $current_page ?? FALSE;
  }

  /**
   * getting the pager if applicable
   *
   * @param $data
   *   JSON response data
   * @param $use_existing
   *
   * @return array|bool
   *  render array or FALSE if no pager
   */
  protected function getPager($use_existing, $pager_size = 0) {
    $pager = [];
    $data = $this->getResponseData();
    if (isset($data->links->first)) {
      $link = Link::createFromRoute($this->t('First'), 'drupal_dam.paginate', ['mediatype' => $this->mediaType->id()], $this->paginationLinkOptions($use_existing, $data->links->first->href));
      $pager['first'] = $link;
    }
    if (isset($data->links->prev)) {
      $link = Link::createFromRoute($this->t('Prev'), 'drupal_dam.paginate', ['mediatype' => $this->mediaType->id()], $this->paginationLinkOptions($use_existing, $data->links->prev->href));
      $pager['prev'] = $link;
    }
    foreach ($this->getNumberedPagination($use_existing, $pager_size) as $pagination_link) {
      $pager[] = $pagination_link;
    }
    if (isset($data->links->next)) {
      $link = Link::createFromRoute($this->t('Next'), 'drupal_dam.paginate', ['mediatype' => $this->mediaType->id()], $this->paginationLinkOptions($use_existing, $data->links->next->href));
      $pager['next'] = $link;
    }
    if ($last = $this->getLastPageLink($use_existing)) {
      $pager['last'] = $last;
    }
    if ($this->resetOption) {
      $options = [
        'query' => [
          'pagination' => $this->paginationCount,
          'useexisting' => (int) $use_existing,
          'allowrecreation' => (int) $this->allowRecreation
        ],
        'attributes' => [
          'class' => ['use-ajax']
        ]
      ];
      $link = Link::createFromRoute($this->t('Reset'), 'drupal_dam.reset', ['mediatype' => $this->mediaType->id()], $options);
      $pager['reset'] = $link;
    }

    return !empty($pager) ? $pager : FALSE;
  }

  /**
   * get Last page link if applicable (search results)
   *
   * @param $use_existing
   *
   * @return \Drupal\Core\Link|false
   */
  protected function getLastPageLink($use_existing) {
    if ($current_page = $this->getCurrentPage()) {
      $data = $this->getResponseData();
      $url_parts = parse_url($data->links->self->href);
      parse_str($url_parts['query'], $query);
      $pages_count = ceil($this->itemCount/$this->paginationCount);
      $page_query = $query;
      $page_query['page']['offset'] = $this->paginationCount * ($pages_count - 1);
      $url_parts['query'] = http_build_query($page_query);
      if ($current_page !== (int) $pages_count) {
        return Link::createFromRoute($this->t('Last'), 'drupal_dam.paginate', ['mediatype' => $this->mediaType->id()], $this->paginationLinkOptions($use_existing, $this->reparse_url($url_parts)));
      }
    }
    return FALSE;
  }

  /**
   * get a numbered pagination if possible
   *
   * @param $use_existing
   * @param $size
   *   size of elements left and right (total size = 2 * $size +1)
   *
   * @return array
   *   array of pagination link render arrays
   */
  protected function getNumberedPagination($use_existing, $size) {
    $links = [];
    if ($size > 0 && $current_page = $this->getCurrentPage()) {
      $data = $this->getResponseData();
      $url_parts = parse_url($data->links->self->href);
      parse_str($url_parts['query'], $query);
      $pages_count = ceil($this->itemCount/$this->paginationCount);
      $pages[] = $current_page;
      for ($i=1;$i<=$size;$i++) {
        if ($current_page - $i > 0) {
          $pages[-$i] = $current_page - $i;
        }
        if ($current_page + $i <= $pages_count) {
          $pages[$i] = $current_page + $i;
        }
      }
      $max_size = $pages_count < $size * 2 +1 ? $pages_count : $size * 2 +1;
      if (count($pages) < $size * 2 + 1 && count($pages) < $pages_count) {
        if ($current_page < $size +1) {
          $max = max($pages);
          $count_pages = count($pages);
          for($j=1;$j<=$max_size-$count_pages;$j++) {
            $pages[] = $max + $j;
          }
        }
        if ($current_page > $pages_count - $size) {
          $min = min($pages);
          $count_pages = count($pages);
          for($j=1;$j<=$max_size-$count_pages;$j++) {
            $pages[] = $min - $j;
          }
        }
      }
      asort($pages);
      foreach ($pages as $page) {
        $page_query = $query;
        $page_query['page']['offset'] = $this->paginationCount * ($page - 1);
        $url_parts['query'] = http_build_query($page_query);
        if ($page == $current_page) {
          $links[] = Link::createFromRoute($page, '<nolink>');
        }
        else {
          $links[] = Link::createFromRoute($page, 'drupal_dam.paginate', ['mediatype' => $this->mediaType->id()], $this->paginationLinkOptions($use_existing, $this->reparse_url($url_parts)));
        }
      }
    }
    return $links;
  }

  /**
   * get link options for pagination link
   *
   * @param $use_existing
   * @param $pagination_link
   *
   * @return array
   *   link options
   */
  protected function paginationLinkOptions($use_existing, $pagination_link) {
    return [
      'query' => [
        'pagination' => $pagination_link,
        'useexisting' => (int) $use_existing,
        'reset' => $this->resetOption,
        'allowrecreation' => (int) $this->allowRecreation,
        'pagination_count' => $this->resetOption ? $this->paginationCount : 0
      ],
      'attributes' => [
        'class' => ['use-ajax']
      ]
    ];
  }

  /**
   * replace parts of internal url with public url
   *
   * @param $inturl array
   *   result from parse_url()
   * @param $puburl array
   *   result from parse_url()
   *
   * @return array
   *   parse_url() style array
   */
  protected function replace_internal_dam_url($inturl, $puburl) {
    if (isset($puburl['scheme'])) {
      $inturl['scheme'] = $puburl['scheme'];
    }
    else {
      unset($inturl['scheme']);
    }
    if (isset($puburl['host'])) {
      $inturl['host'] = $puburl['host'];
    }
    if (isset($puburl['user'])) {
      $inturl['user'] = $puburl['user'];
    }
    else {
      unset($inturl['user']);
    }
    if (isset($puburl['pass'])) {
      $inturl['pass'] = $puburl['pass'];
    }
    else {
      unset($inturl['pass']);
    }
    if (isset($puburl['port'])) {
      $inturl['port'] = $puburl['port'];
    }
    else {
      unset($inturl['port']);
    }
    if (!empty($puburl['path'])) {
      $inturl['path'] = rtrim($puburl['path'], '/') . $inturl['path'];
    }
    return $inturl;
  }

  /**
   * helper function to create uri from parse_url() style array
   *
   * @param $url array
   *   result from parse_url
   *
   * @return string
   *   uri
   */
  public function reparse_url($url) {
    $reparsed_url = '';
    if (isset($url['scheme'])) {
      $reparsed_url .= $url['scheme'];
    }
    $reparsed_url .= '://';
    if (isset($url['user'])) {
      $reparsed_url .= $url['user'].':';
      $reparsed_url .= $url['pass'].'@';
    }
    if (isset($url['host'])) {
      $reparsed_url .= $url['host'];
    }
    if (isset($url['port'])) {
      $reparsed_url .= ':'.$url['port'];
    }
    if (isset($url['path'])) {
      $reparsed_url .= $url['path'];
    }
    if (isset($url['query'])) {
      $reparsed_url .= '?'.$url['query'];
    }
    if (isset($url['fragment'])) {
      $reparsed_url .= '#'.$url['fragment'];
    }
    return $reparsed_url;
  }
}
